create database pet_society;

\c pet_society;

create table vet_clinic(
    id SERIAL primary key,
    name VARCHAR(60) not null,
    address VARCHAR(255) not null,
    district VARCHAR(60) not null,
    photo VARCHAR(255),
    phone TEXT not null,
    opening_hour TIME not null,
    closing_hour TIME not null,
    details TEXT,
    reservation boolean,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table "user"(
    id SERIAL primary key,
    username VARCHAR(20) not null,
    hashed_password VARCHAR(60),
    email VARCHAR(255) not null,
    icon VARCHAR(255),
    address VARCHAR(255),
    is_vet_clinic_user boolean not null,
    vet_clinic_id INTEGER references vet_clinic(id),
    is_admin boolean not null,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table pet(
    id SERIAL primary key,
    user_id INTEGER not null references "user"(id),
    name VARCHAR(20) not null,
    icon VARCHAR(255),
    type varchar(60) not null,
    breed VARCHAR(60) not null,
    sex VARCHAR(6) not null,
    birthday DATE,
    neutered boolean,
    weight float,
    chip_number VARCHAR(60),
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table vet(
    id SERIAL primary key,
    name VARCHAR(60) not null,
    photo VARCHAR(255),
    vet_clinic_id INTEGER not null references vet_clinic(id),
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table medical_history(
    id SERIAL primary key,
    pet_id INTEGER not null references pet(id),
    date DATE not null,
    name VARCHAR(60) not null,
    type VARCHAR(60),
    photo VARCHAR(255),
    details TEXT,
    vet_id INTEGER references vet(id),
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table comment_on_vet_clinic(
    id SERIAL primary key,
    user_id INTEGER not null references "user"(id),
    vet_clinic_id INTEGER not null references vet_clinic(id),
    topic VARCHAR(255) not null,
    date DATE not null,
    content TEXT not null,
    photo VARCHAR(255),
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table vaccination_record(
    id SERIAL primary key,
    pet_id INTEGER not null references pet(id),
    date DATE not null,
    name VARCHAR(60) not null,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table reply_on_comment(
    id SERIAL primary key,
    user_id INTEGER not null references "user"(id),
    comment_on_vet_clinic_id INTEGER not null references comment_on_vet_clinic(id),
    content TEXT not null,
    created_at TIMESTAMP not null,
    updated_at TIMESTAMP not null
);

create table likes_on_vet_clinic(
    id SERIAL primary key,
    user_id INTEGER not null references "user"(id),
    vet_clinic_id INTEGER not null references vet_clinic(id),
    created_at TIMESTAMP not null
);

create table contact_us_info(
    id SERIAL primary key,
    name VARCHAR(60) not null,
    email VARCHAR(255) not null,
    content TEXT not null,
    phone INTEGER not null,
    created_at TIMESTAMP not null
);

-- create table comment_on_vet(
--     id SERIAL primary key,
--     user_id INTEGER not null references "user"(id),
--     vet_id INTEGER not null references vet(id),
--     topic VARCHAR(255) not null,
--     date DATE not null,
--     content TEXT not null,
--     photo VARCHAR(255),
--     created_at TIMESTAMP not null,
--     updated_at TIMESTAMP not null
-- );
