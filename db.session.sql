select * from comment_on_vet_clinic;

select * from reply_on_comment;

select comment_on_vet_clinic.* , 
  json_agg(reply_on_comment.content) as comment from comment_on_vet_clinic
  LEFT JOIN reply_on_comment ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
  GROUP BY comment_on_vet_clinic.id;


select comment_on_vet_clinic.* , 
  json_agg( 
      json_build_object(
      'comment', reply_on_comment.content,
      'update',reply_on_comment.created_at, 
      'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id) ) ) as reply_on_comment
  from comment_on_vet_clinic
  LEFT JOIN reply_on_comment 
  ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
  where comment_on_vet_clinic.vet_clinic_id = 4
  GROUP BY comment_on_vet_clinic.id;


select comment_on_vet_clinic.* , 
  json_agg( json_build_object('comment', roc.content, 'user_id', roc.user_id, 'update',roc.created_at ) ) as comment,
    "user".icon,"user".username
    from "user"
    right join comment_on_vet_clinic 
    on comment_on_vet_clinic.user_id  = "user".id
    LEFT JOIN reply_on_comment roc
    ON comment_on_vet_clinic.id  = roc.comment_on_vet_clinic_id 
    right join "user"
    on "user".id = roc.user_id
    where comment_on_vet_clinic.vet_clinic_id = ${id}
    GROUP BY comment_on_vet_clinic.id,"user".id
    ORDER BY comment_on_vet_clinic.date
    );


select comment_on_vet_clinic.* , 
  json_agg( 
    json_build_object(
      'comment', roc.content,
      'user_id', roc.user_id,
      'update',roc.created_at,
      'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id) 
    ) 
  ) as comment,
  "user".icon,"user".username
  from "user"
  right join comment_on_vet_clinic 
  on comment_on_vet_clinic.user_id  = "user".id
  LEFT JOIN reply_on_comment roc
  ON comment_on_vet_clinic.id  = roc.comment_on_vet_clinic_id 
  where comment_on_vet_clinic.vet_clinic_id = ${id}
  GROUP BY comment_on_vet_clinic.id,"user".id
  ORDER BY comment_on_vet_clinic.date
  );


 select comment_on_vet_clinic.* ,
    "user".icon,
    "user".username,
    json_agg( 
      json_build_object(
        'comment', reply_on_comment.content,
        'update',reply_on_comment.created_at, 
        'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id) 
      ) 
    ) as comment
    from comment_on_vet_clinic
    join "user"
    on comment_on_vet_clinic.user_id  = "user".id
    LEFT JOIN reply_on_comment 
    ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
    where comment_on_vet_clinic.vet_clinic_id = $1
    GROUP BY comment_on_vet_clinic.id, "user".icon, "user".username;
    `,[id]);

    