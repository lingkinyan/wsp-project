import dotenv from "dotenv";

dotenv.config();

let env = {
  DB_NAME: process.env.DB_NAME || "petone-db",
  DB_USERNAME: process.env.DB_USERNAME || "petone",
  DB_PASSWORD: process.env.DB_PASSWORD || "petone",
  TEST_DB_NAME: process.env.DB_NAME || "petone-db-test",
  TEST_DB_USERNAME: process.env.DB_USERNAME || "postgres",
  TEST_DB_PASSWORD: process.env.DB_NAME || "postgres",
  SERVER_PORT: process.env.SERVER_PORT || 8080,
  NODE_ENV: process.env.NODE_ENV || "development",
  SESSION_SECRET: process.env.SESSION_SECRET || "canwefinish",
};

export default env;
