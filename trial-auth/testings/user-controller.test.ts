import { Request, Response } from "express";
import { UserController } from "../controllers/user-controller";
import knex from "../knex";
import { UserService } from "../services/user-service";

jest.mock("../services/user-service");

describe("UserController", () => {
  let controller: UserController;
  let service: UserService;
  let resJsonSpy: jest.SpyInstance;
  let req: Request;
  let res: Response;

  beforeEach(function () {
    console.error = jest.fn();
    console.log = jest.fn();
    service = new UserService(knex);
    service.getVetClinicList = jest.fn(() =>
      Promise.resolve([
        { id: 1, name: "so9sad vet clinic" },
        { id: 2, name: "so9happy vet clinic" },
      ])
    );
    service.checkUserByUsername = jest.fn((username: string) =>
      Promise.resolve([])
    );
    service.checkUserByEmail = jest.fn((email: string) => Promise.resolve([]));
    service.insertNewUser = jest.fn(
      (
        username: string,
        email: string,
        hashedPassword: string,
        vetClinicID: number | null
      ) => Promise.resolve([{ id: 1 }])
    );
    service.checkUserByUsernameForSessionSave = jest.fn((username: string) =>
      Promise.resolve([])
    );
    service.checkVetClinicByName = jest.fn((vetClinicName: string) =>
      Promise.resolve([])
    );
    service.checkVetClinicByPhone = jest.fn((vetClinicPhone: string) =>
      Promise.resolve([])
    );
    service.insertNewVetClinic = jest.fn(
      (
        vetClinicName: string,
        vetClinicAddress: string,
        vetClinicDistrict: string,
        vetClinicPhone: string,
        vetClinicOpeningHour: string,
        vetClinicClosingHour: string
      ) => Promise.resolve([{ id: 10 }])
    );

    controller = new UserController(service);
    // req = {
    //   body: {},
    //   params: {},
    //   session: {
    //     user: {
    //       id: 1,
    //     },
    //   },
    // } as any as Request;

    res = {
      redirect: () => {},
      json: () => {},
      status: () => {
        return {
          json: () => {},
        };
      },
    } as any as Response;

    resJsonSpy = jest.spyOn((res as any).status(), "json");
  });

  it.skip("should load vet clinic list correctly", async () => {
    const mockGetVetClinicList = jest.spyOn(service, "getVetClinicList");
    req = {
      body: {},
      params: {},
      session: {
        user: {
          id: 1,
        },
      },
    } as any as Request;
    afterEach(function () {
      req = {
        body: {},
        params: {},
        session: {
          user: {
            id: 1,
          },
        },
      } as any as Request;
    });
    await controller.loadVetClinicList(req, res);
    expect(mockGetVetClinicList.mock.calls.length).toBe(1);
    expect(resJsonSpy).toBeCalledWith({
      clinicList: [
        { id: 1, name: "so9sad vet clinic" },
        { id: 2, name: "so9happy vet clinic" },
      ],
    });
  });

  describe("registering", () => {
    describe("normal user", () => {
      req = {
        body: {
          username: "testing1",
          email: "testing1@gmail.com",
          password: "testing1",
          checkboxForVetClinicUser: false,
          vetClinicID: -1,
        },
      } as any as Request;

      it("go through username checking", async () => {
        const mockCheckUserByUsername = jest.spyOn(
          service,
          "checkUserByUsername"
        );
        await controller.register(req, res);
        expect(mockCheckUserByUsername.mock.calls.length).toBe(1);
      });

      it("go through email checking", async () => {
        const mockCheckUserByEmail = jest.spyOn(service, "checkUserByEmail");
        await controller.register(req, res);
        expect(mockCheckUserByEmail.mock.calls.length).toBe(1);
      });

      it("insert new user successfully", async () => {
        const mockInsertNewUser = jest.spyOn(service, "insertNewUser");
        await controller.register(req, res);
        expect(mockInsertNewUser.mock.calls.length).toBe(1);
      });

      it("save session info", async () => {
        const mockCheckUserByUsernameForSessionSave = jest.spyOn(
          service,
          "checkUserByUsernameForSessionSave"
        );
        await controller.register(req, res);
        expect(mockCheckUserByUsernameForSessionSave.mock.calls.length).toBe(1);
      });
    });

    describe("vet clinic user with registered clinic", () => {
      req = {
        body: {
          username: "testing2",
          email: "testing2@gmail.com",
          password: "testing2",
          checkboxForVetClinicUser: true,
          vetClinicID: 2,
        },
      } as Request;

      it("go through username checking", async () => {
        const mockCheckUserByUsername = jest.spyOn(
          service,
          "checkUserByUsername"
        );
        await controller.register(req, res);
        expect(mockCheckUserByUsername.mock.calls.length).toBe(1);
      });

      it("go through email checking", async () => {
        const mockCheckUserByEmail = jest.spyOn(service, "checkUserByEmail");
        await controller.register(req, res);
        expect(mockCheckUserByEmail.mock.calls.length).toBe(1);
      });

      // it("insert new user successfully", async () => {
      //   const mockInsertNewUser = jest.spyOn(service, "insertNewUser");
      //   await controller.register(req, res);
      //   expect(mockInsertNewUser.mock.calls.length).toBe(1);
      // });

      it("save session info", async () => {
        const mockCheckUserByUsernameForSessionSave = jest.spyOn(
          service,
          "checkUserByUsernameForSessionSave"
        );
        await controller.register(req, res);
        expect(mockCheckUserByUsernameForSessionSave.mock.calls.length).toBe(1);
      });
    });

    describe("vet clinic user with unregistered clinic", () => {
      req = {
        body: {
          username: "testing3",
          email: "testing3@gmail.com",
          password: "testing3",
          checkboxForVetClinicUser: true,
          vetClinicID: 0,
          vetClinicName: "bilibala vet clinic",
          vetClinicAddress: "tw hk",
          vetClinicDistrict: "tw",
          vetClinicPhone: "12345678",
          vetClinicOpeningHour: "08:00:00",
          vetClinicClosingHour: "22:00:00",
        },
      } as Request;

      it("go through vet clinic name checking", async () => {
        const mockCheckVetClinicByName = jest.spyOn(
          service,
          "checkVetClinicByName"
        );
        await controller.register(req, res);
        expect(mockCheckVetClinicByName.mock.calls.length).toBe(1);
      });

      it("go through vet clinic phone checking", async () => {
        const mockCheckVetClinicByPhone = jest.spyOn(
          service,
          "checkVetClinicByPhone"
        );
        await controller.register(req, res);
        expect(mockCheckVetClinicByPhone.mock.calls.length).toBe(1);
      });

      it("go through username checking", async () => {
        const mockCheckUserByUsername = jest.spyOn(
          service,
          "checkUserByUsername"
        );
        await controller.register(req, res);
        expect(mockCheckUserByUsername.mock.calls.length).toBe(1);
      });

      it("go through email checking", async () => {
        const mockCheckUserByEmail = jest.spyOn(service, "checkUserByEmail");
        await controller.register(req, res);
        expect(mockCheckUserByEmail.mock.calls.length).toBe(1);
      });

      it("insert new vet clinic successfully", async () => {
        const mockInsertNewVetClinic = jest.spyOn(
          service,
          "insertNewVetClinic"
        );
        await controller.register(req, res);
        expect(mockInsertNewVetClinic.mock.calls.length).toBe(1);
      });
    });
  });
});
