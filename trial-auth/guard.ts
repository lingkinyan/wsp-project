import { Request, Response, NextFunction } from "express";
import "./session";

let testUser = { id: 1 } as any;
testUser = undefined;

export function loggedIn(req: Request, res: Response, next: NextFunction) {
  if (!req.session.user) {
    req.session.user = testUser;
    req.session.save();
  }
  if (req.session.user) {
    next();
  } else {
    if (req.url.endsWith(".html")) {
      res.redirect("/login.html");
    } else {
      res.status(401).json({ message: "請先登錄，以使用此功能" });
    }
    // res.redirect('/login.html')
    // res.status(401).json({ message: '請先登錄，以使用此功能' });
  }
}

// export function loggedInClinic (req: Request, res: Response, next: NextFunction) {
//     if(!req.session.user){
//         req.session.user = testUser
//         req.session.save()
//     }
//     if (req.session.user) {
//         next();
//     } else {
//         // res.status(401).json({ message: '請先登錄，以使用此功能' });
//         res.redirect('/login.html')
//         // res.redirect('/login.html')
//         // res.status(401).json({ message: '請先登錄，以使用此功能' });
//     }
// }

export function loggedInOrRedirect(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (!req.session.user) {
    req.session.user = testUser;
    req.session.save();
  }
  if (req.session.user) {
    next();
  } else {
    res.redirect("/login.html");
  }
}

export function vetClinicUserOnly(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.session.user?.is_vet_clinic_user) {
    next();
  } else {
    res.status(401).json({ message: "這是獸醫診所用戶的專區" });
  }
}

export function adminOnly(req: Request, res: Response, next: NextFunction) {
  if (req.session.user?.is_admin) {
    next();
  } else {
    res.status(401).json({ message: "管理員專用！請先登錄" });
  }
}
