import multer from "multer";
import path from "path";

// fileCounter 用黎怕撞名 所以要++
let fileCounter = 0;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    fileCounter++;
    //呢部份係處理d照片格式名有時係有（- ；)
    let ext: string = file.mimetype.split("/").pop()!;
    ext = ext?.split("-").pop()!;
    ext = ext.split(";")[0];
    //利用callback to throw error
    if (ext != "jpeg" && ext != "png" && ext != "tiff" && ext !="jpg") {
      cb(new Error("Invalid photo"), null as any);
      //利用req.res令句error 靚仔少少
      req.res?.status(400).end("Invalid photo, only allow png format");
    } else {
      cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`);
    }
  },
});

const upload = multer({ storage });

export const multerSingleImage = upload.single("photo");