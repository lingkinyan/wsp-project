import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
  let password = "123456";
  const hashedPassword = await hashPassword(password);
  // Deletes ALL existing entries
  await knex("vet_clinic_business_hour").del();
  await knex("vet_clinic").del();

  // Inserts seed entries
  // await knex("vet_clinic").insert([
  //     { name:"test clinic1", address: "hk", district: "hk",phone:"23332222",opening_hour: "06:00:00",closing_hour: "18:00:00",details:"good" },
  //     { name:"test clinic2", address: "hk", district: "hk",phone:"23332222",opening_hour: "06:00:00",closing_hour: "18:00:00",details:"good" },
  //     // { id: 3, colName: "rowValue3" }
  // ]);

  await knex("user").insert([
    {
      username: "admin1",
      hashed_password: `${hashedPassword}`,
      email: "admin@gmail.com",
      is_vet_clinic_user: false,
      is_admin: true,
    },

    // { id: 3, colName: "rowValue3" }
  ]);
}
