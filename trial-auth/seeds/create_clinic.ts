import { Knex } from "knex";
import xlsx from "xlsx";

class list {
  chinese_district: string = "";
  english_district: string = "";
  english_name: string = "";
  chinese_name: string = "";
  english_address: string = "";
  chinese_address: string = "";
  tel: number = 0;
  IP: string = "";
  is_default: boolean = true;
  monday_start_time: string = "";
  monday_end_time: string = "";
  monday_start_time2: string = "";
  monday_end_time2: string = "";
  tuesday_start_time: string = "";
  tuesday_end_time: string = "";
  tuesday_start_time2: string = "";
  tuesday_end_time2: string = "";
  wednesday_start_time: string = "";
  wednesday_end_time: string = "";
  wednesday_start_time2: string = "";
  wednesday_end_time2: string = "";
  thursday_start_time: string = "";
  thursday_end_time: string = "";
  thursday_start_time2: string = "";
  thursday_end_time2: string = "";
  friday_start_time: string = "";
  friday_end_time: string = "";
  friday_start_time2: string = "";
  friday_end_time2: string = "";
  saturday_start_time: string = "";
  saturday_end_time: string = "";
  saturday_start_time2: string = "";
  saturday_end_time2: string = "";
  sunday_start_time: string = "";
  sunday_end_time: string = "";
  sunday_start_time2: string = "";
  sunday_end_time2: string = "";
}

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("vet_clinic_business_hour").del();
  await knex("vet_clinic").del();

  const xlsxFile = xlsx.readFile("./vetlist_2021.xlsx");

  const txn = await knex.transaction();
  const lists: list[] = xlsx.utils.sheet_to_json(xlsxFile.Sheets["list"]);
  console.log("first item:", lists[0]);
  try {
    for (let list of lists) {
      const [vet_clinic_id] = await knex("vet_clinic")
        .insert({
          chinese_name: `${list.chinese_name}`,
          english_name: `${list.english_name}`,
          english_district: `${list.english_district}`,
          chinese_district: `${list.chinese_district}`,
          chinese_address: `${list.chinese_address}`,
          english_address: `${list.english_address}`,
          phone: `${list.tel}`,
          IP: `${list.IP}`,
        })
        .returning("id");

      if (list.is_default) {
        await knex("vet_clinic_business_hour").insert({
          vet_clinic_id: vet_clinic_id.id,
          is_default: `${list.is_default}`,
          monday_start_time: `${list.monday_start_time}`,
          monday_end_time: `${list.monday_end_time}`,
          monday_start_time2: `${list.monday_start_time2}`,
          monday_end_time2: `${list.monday_end_time2}`,
          tuesday_start_time: `${list.tuesday_start_time}`,
          tuesday_end_time: `${list.tuesday_end_time}`,
          tuesday_start_time2: `${list.tuesday_start_time2}`,
          tuesday_end_time2: `${list.tuesday_end_time2}`,
          wednesday_start_time: `${list.wednesday_start_time}`,
          wednesday_end_time: `${list.wednesday_end_time}`,
          wednesday_start_time2: `${list.wednesday_start_time2}`,
          wednesday_end_time2: `${list.wednesday_end_time2}`,
          thursday_start_time: `${list.thursday_start_time}`,
          thursday_end_time: `${list.thursday_end_time}`,
          thursday_start_time2: `${list.thursday_start_time2}`,
          thursday_end_time2: `${list.thursday_end_time2}`,
          friday_start_time: `${list.friday_start_time}`,
          friday_end_time: `${list.friday_end_time}`,
          friday_start_time2: `${list.friday_start_time2}`,
          friday_end_time2: `${list.friday_end_time2}`,
          saturday_start_time: `${list.saturday_start_time}`,
          saturday_end_time: `${list.sunday_end_time}`,
          saturday_start_time2: `${list.saturday_start_time2}`,
          saturday_end_time2: `${list.sunday_end_time2}`,
          sunday_start_time: `${list.sunday_start_time}`,
          sunday_end_time: `${list.sunday_end_time}`,
          sunday_start_time2: `${list.sunday_start_time2}`,
          sunday_end_time2: `${list.sunday_end_time2}`,
        });
      }
    }
  } catch (error) {
    console.log(error);
    await txn.rollback();
  }
}
