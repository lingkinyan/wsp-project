npm init --yes

npm install \
  express \
  listening-on \
  express-session \
  dotenv

npm install -D \
  ts-node \
  typescript \
  ts-node-dev \
  @types/node \
  @types/express \
  @types/express-session

npm set-script test "ts-node-dev server.ts"

mkdir -p public
echo '404 page not found' > public/404.html

echo '
node_modules
package-lock.json
pnpm-lock.yaml
.env
' > .gitignore

echo '{
  "compilerOptions": {
    "strict": true,
    "module": "commonjs",
    "target": "es5",
    "lib": ["es6", "dom"],
    "sourceMap": true,
    "allowJs": true,
    "jsx": "react",
    "esModuleInterop": true,
    "moduleResolution": "node",
    "noImplicitReturns": true,
    "noImplicitThis": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "suppressImplicitAnyIndexErrors": true,
    "noUnusedLocals": true
  },
  "exclude": ["node_modules", "build", "scripts", "index.js"]
}
' > tsconfig.json