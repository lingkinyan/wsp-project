const logoutForm = document.querySelector("#logoutButton");

logoutForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  console.log("clicked logout button");
  
  try {
    let res = await fetch("/logout", {
      method: "POST",
    });
    let json = await res.json();
    console.log("message:", json.message);
    setTimeout(() => {
      window.location.href = "/index.html";
    }, 2000);
  } catch (error) {
      console.log("message:", error.toString());
  }
});


async function findName(){
  let res = await fetch('/loginstatus')
  let json = await res.json();
  // console.log(json);

  let login = document.querySelector('.login')
  let welcome = document.querySelector('.welcome')
  let register = document.querySelector('.register')
  if(json){
    // console.log('login');
    welcome.innerHTML = `您好, ${json}!`
    register.style.display = 'none'
    login.style.display = 'none'
    return;
  }
  
  if (!json){
    // console.log('not login');
    let logout = document.querySelector('#logoutButton')
    logout.style.display= 'none'
  }
}
findName();