const searchParams = new URLSearchParams(location.search)
const id = searchParams.get('id')

let toggle1 = document.querySelector(".replyBox")
let toggle2 = document.querySelector(".replySubmitButton")
let createMemo = document.querySelector("#boxGroup")
let memoContainer = document.querySelector(".boxGroup1")
let introContainer = document.querySelector(".boxGroup2")
let memoContainer1 = document.querySelector(".replyGroup")
let detailContainer = document.querySelector(".clinicDetail")
let deleteMemo = document.querySelector(".delButton")
let likeButtonContainer = document.querySelector(".like")
let likeButton = document.querySelector(".likeButton")

function likeNumber(json) {
    document.querySelector("#likesNum").innerHTML = `${json.length}`;
}

async function loadLike() {
    let res = await fetch('/like?id=' + id)
    let json = await res.json()
    console.log(json);

    // for (let memo of json) {
        likeNumber(json)
    //     console.log(memo)
    // }
}

loadLike()

likeButton.addEventListener('click', async function (event) {
    event.preventDefault(); //停左submit功能 做下面的ajax
    console.log('do some AJAX pic')
    // Serialize the Form afterwards
    const formObject = {};
    const form = event.target;

    formObject['vet_clinic_id'] = id;
    console.log(id);

    // console.log('formObject :', formData);
    let msgBox = document.querySelector('.likeMsgBox')

    const res = await fetch("/like", {
        method: "POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObject)
    });
    const result = await res.json();

    // loadLike()
    msgBox.textContent = result.message
    if (200 <= res.status && res.status < 300) {
        loadLike()
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
        // reset()
    } else {
    //     msgBox.classList.add('error')
    //     msgBox.classList.remove('success')
    }
    // // document.querySelector('.likeMsgBox').innerHTML = result;
    // msgBox.textContent = result.message
})


function showClinicIntro(memo) {
    let date = new Date().toDateString();
    let div = document.createElement('div')
    div.className = ""

    div.innerHTML = /* html */`
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        ${memo.details}
                    </div>
                </div>
            </div>
            `
    introContainer.appendChild(div)
}

async function loadIntro(id) {
    let res = await fetch('/vet_clinic?id=' + id)
    console.log(res)
    let json = await res.json()
    console.log(json)
    //要清空返 如果唔係一加新野 舊果d又重覆loop一次
    introContainer.innerHTML = ""
    json.reverse()
    for (let memo of json) {
        showClinicIntro(memo)
        console.log(memo)
    }
}

loadIntro(`${id}`)


function showClinicDetail(memo) {
    let date = new Date().toDateString();
    let div = document.createElement('div')
    div.className = "clinic"

    div.innerHTML = /* html */`
            <div class="container">
                <div class="row">
                    <div class="with-forms basicUpper">
                        <div class="row upperInfo">
                            <div class="col-md-6 textBox">              
                                <div id="iconPhoto"><img src="${memo.photo?"/clinicPhoto/" + memo.photo:"https://previews.123rf.com/images/milankivi/milankivi1612/milankivi161200059/69508538-veterinary-clinic-banner-with-dog-and-laboratory-on-background.jpg"}"></div>

                            </div>
                            <div class="col-md-6 textBox">  
                                <div>診所名稱：${memo.chinese_name}</div>
                            <div class="textBox">
                                <div>診所地址：${memo.chinese_address}</div>
                            </div>
                            <div class="image textBox">
                                <div>地區：${memo.chinese_district}</div>
                                <div>電話：${memo.phone}</div>
                            </div>
                                <div>營業時間：${memo.opening_hour}-${memo.closing_hour}</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            `
    detailContainer.appendChild(div)
}

async function loadDetail(id) {
    let res = await fetch('/vet_clinic?id=' + id)
    let json = await res.json()
    //要清空返 如果唔係一加新野 舊果d又重覆loop一次
    detailContainer.innerHTML = ""
    json.reverse()
    for (let memo of json) {
        showClinicDetail(memo)
        console.log(memo)
    }
}

loadDetail(`${id}`)

let onlyDate = JSON.stringify(new Date())
let date1 = onlyDate.slice(0,10)

function showMemo(memo) {
    let date = new Date().toDateString();
    let div = document.createElement('div')
    div.className = "boxGroup"
    console.log({ "url": memo.photo })
    div.innerHTML = /* html */`
<div class ="row">
        <div class="col-md-3">
            ${date}
        <div>
            ${memo.username}
        </div>
        <div>
        <img src="${memo.icon?"/usericons/" + memo.icon:"https://png.pngtree.com/png-vector/20190130/ourlarge/pngtree-simple-and-cute-cartoon-people-and-cute-pets-petfresh-png-image_592461.jpg"}" class="userIcon">
        </div>
        </div>
    <div class="col-md-9">
        <div class=""><h5> ${memo.topic}</h5></div>
        <div></div>
        <div> ${memo.content}</div>
         ${memo.photo ? `<img src="/uploads/${memo.photo}">` : ""}
        <div class="buttonGroup">
            <button id="del-${memo.id}" class="delButton">
            Delete
            <i class="fas fa-trash"></i>
            </button>
            <button id="rev-${memo.id}" class="reviseButton">
            Revise
            <i class="far fa-edit"></i> 
            </button>
            <button id="reply-${memo.id}" class="replyButton">
            reply
                <i class="fas fa-reply"></i>
            </button>
            <div>
            <p class="msg-box"></p>
            </div>
        </div>
        
        <form action="/revise_on_vet_clinic" method="PUT" id="" class="">
        <div id="revise-${memo.id}" class="reviseBox1">
        <div> Revise </div>
            <label for="reviseContent" class="reviseContentLabel">
            <textarea name="reviseContent" class="reviseContent" id="reviseContent-${memo.id}" placeholder="輸入你的回覆">${memo.content}</textarea>
            <div>
            <input class="reviseSubmitButton" id="reviseButton-${memo.id}" type='submit' value="提交" />
            </div>
            </label>
            </div>
        </form>

        <form action="/reply_on_vet_clinic" method="POST" id="" class="">
        <div id="comment-${memo.id}" class="replyBox1">
        <div> Reply </div>
            <label for="replyContent" class="replyContentLabel">
            <textarea name="replyContent" class="replyContent" id="replyContent-${memo.id}" placeholder="輸入你的回覆"></textarea>
            <div>
            <input class="replySubmitButton" id="replyButton-${memo.id}" type='submit' value="提交" />
            </div>
            </label>
            </div>
        </form>

    <div class="replyGroup"> 
            <div>
            </div>
        <div class="replyUnderBox">

            ${memo.comment.map((v, i) => {
                if (v.comment != null) {
                    let date = JSON.stringify(`${v.update}`)
                    let date1 = date.slice(1,11)
                    let minute = date.slice(12,20)

                    let realDate = date1 +" "+ minute

                    return `
                    <p>${v.username}: 
                    ${v.comment}    
                    </p>
                    <div>${realDate}
                    </div>
                    `
                }
                return ``
            })}
        </div>
    </div>
    </div>
</div>
        `
    memoContainer.appendChild(div)
}

async function loadMemo() {
    let res = await fetch(`/comment_on_vet_clinic/${id}`)

    // let res1 = await fetch('/reply_on_comment?id=1')
    let json = await res.json()

    // let json1 = await res1.json()
    //要清空返 如果唔係一加新野 舊果d又重覆loop一次
    memoContainer.innerHTML = ""
    json.reverse()
    for (let memo of json) {
        showMemo(memo)
        console.log(memo)
    }

    //set this for loop the memo id for edit/delete
    let deleteMemos = document.querySelectorAll(".delButton")
    for (let memo1 of deleteMemos) {
        //let deleteMemo = memo.querySelector(".delButton")
        memo1.addEventListener('click', async function (event) {
            event.preventDefault();
            console.log('do some AJAX pic')
            // const formObject = {};
            // const form = event.target;
            console.log(memo1)
            console.log(memo1.id)
            const id = parseInt(memo1.id.replace("del-", ""))
            const res = await fetch(`/comment_on_vet_clinic/${id}`, {
                method: "DELETE",
            });
            let msgBox = document.querySelector('.msg-box')
            const result = await res.json();
            msgBox.textContent = result.message
            if (200 <= res.status && res.status < 300) {
                loadMemo()
                // msgBox.classList.add('success')
                // msgBox.classList.remove('error')
            } else {
                // msgBox.classList.add('error')
                // msgBox.classList.remove('success')
            }
        })
    }


    let reviseMemos = document.querySelectorAll('.reviseButton')
    for (let revise of reviseMemos) {
        revise.addEventListener('click', async function (event) {
            // let id1 = parseInt(revise.id.replace("rev-",""))
            const id = parseInt(revise.id.replace("rev-", ""))

            console.log(revise.id)
            document.querySelector(`#${revise.id}`).classList.toggle('replyButtonShow')
            document.querySelector(`#revise-${id}`).classList.toggle('reviseBox1')
            // document.querySelector('.replySubmitButton').classList.toggle('replyButtonShow')
            document.querySelector('.reviseSubmitButton').classList.toggle('reviseSubmitButton')
            event.preventDefault();
            console.log('do some AJAX pic')

            // const res = await fetch(`/comment_on_vet_clinic/${id}`, {
            //     method: "PUT",
            // });
        })
    }

    let reviseSubmitButtons = document.querySelectorAll('.reviseSubmitButton')
    for (let reviseButton of reviseSubmitButtons) {
        reviseButton.addEventListener('click', async function (event) {
            event.preventDefault(); //停左submit功能 做下面的ajax
            console.log('do some AJAX pic')
            // Serialize the Form afterwards
            //const formObject = {};

            const id = parseInt(reviseButton.id.replace("reviseButton-", ""))
            console.log('reviseButton ID :', id);
            const form = document.querySelector(`#reviseContent-${id}`);
            console.log(form.value);
            // (debug)new FormData入面有個(form) ->放埋字入去 

            //formObject['replyContent'] = form.value;
            //let msgBox = form.querySelector('.msg-box')
            // console.log(formObject);
            // formData.append('photo',form.photo.files[0])
            // formObject['secondInput'] = form.secondInput.value;
            // formObject['thirdInput'] = form.thirdInput.value;

            let fData = new FormData();
            fData.append("content", form.value)
            fData.append("comment_on_vet_clinic_id", id)
            for (var value of fData.values()) {
                console.log('fData :', value);
             }
            const res = await fetch(`/comment_on_vet_clinic/${id}`, {
                method: "PUT",
                body: fData
            });
            let msgBox = document.querySelector('.msg-box')
            const result = await res.json();
            console.log('Result :', result);
            msgBox.textContent = result.message
            if (200 <= res.status && res.status < 300) {
                loadMemo()
                // msgBox.classList.add('success')
                // msgBox.classList.remove('error')
                // form.reset()
            } else {
                // msgBox.classList.add('error')
                // msgBox.classList.remove('success')
            }
        })
    }

    let replyButtons = document.querySelectorAll('.replyButton')
    for (let reply of replyButtons) {
        reply.addEventListener('click', async function (event) {
            // let id1 = parseInt(revise.id.replace("rev-",""))
            const id = parseInt(reply.id.replace("reply-", ""))

            console.log(reply.id)
            document.querySelector(`#${reply.id}`).classList.toggle('replyButtonShow')
            document.querySelector(`#comment-${id}`).classList.toggle('replyBox1')
            // document.querySelector('.replySubmitButton').classList.toggle('replyButtonShow')
            document.querySelector('.replySubmitButton').classList.toggle('replySubmitButton')
            event.preventDefault();
            console.log('do some AJAX pic')


            // const res = await fetch(`/comment_on_vet_clinic/${id}`, {
            //     method: "PUT",
            // });
        })
    }

    let replySubmitButtons = document.querySelectorAll('.replySubmitButton')
    for (let replyButton of replySubmitButtons) {
        replyButton.addEventListener('click', async function (event) {
            event.preventDefault(); //停左submit功能 做下面的ajax
            console.log('do some AJAX pic')
            // Serialize the Form afterwards
            //const formObject = {};

            const id = parseInt(replyButton.id.replace("replyButton-", ""))
            console.log('replyButton ID :', id);
            const form = document.querySelector(`#replyContent-${id}`);
            // (debug)new FormData入面有個(form) ->放埋字入去 

            //formObject['replyContent'] = form.value;
            //let msgBox = form.querySelector('.msg-box')
            // console.log(formObject);
            // formData.append('photo',form.photo.files[0])
            // formObject['secondInput'] = form.secondInput.value;
            // formObject['thirdInput'] = form.thirdInput.value;

            let fData = new FormData();
            fData.append("replyContent", form.value)
            fData.append("comment_on_vet_clinic_id", id)

            const res = await fetch('/reply_on_vet_clinic', {
                method: "POST",
                body: fData
            });
            let msgBox = document.querySelector('.msg-box')
            const result = await res.json();
            msgBox.textContent = result.message
            if (200 <= res.status && res.status < 300) {
                loadMemo()
                // msgBox.classList.add('success')
                // msgBox.classList.remove('error')
                // form.reset()
            } else {
                // msgBox.classList.add('error')
                // msgBox.classList.remove('success')
            }
        })
    }
}
loadMemo()


createMemo.addEventListener('submit', async function (event) {
    event.preventDefault(); //停左submit功能 做下面的ajax
    console.log('do some AJAX pic')
    // Serialize the Form afterwards
    const formObject = {};
    const form = event.target;
    // (debug)new FormData入面有個(form) ->放埋字入去 
    const formData = new FormData();
    formData.append('topic1', form.topic.value)
    formData.append('content', form.content.value)
    formData.append('vet_clinic_id', id)

    // formObject['topic'] = form.topic.value;
    // formObject['content'] = form.content.value;
    // formObject['vet_clinic_id'] = id;
    console.log('formObject :', formData);
    let msgBox = form.querySelector('.msg-box')

    formData.append('photo', form.photo.files[0])
    // formObject['secondInput'] = form.secondInput.value;
    // formObject['thirdInput'] = form.thirdInput.value;
    const res = await fetch('/comment_on_vet_clinic', {
        method: "POST",
        body: formData
    });
    const result = await res.json();
    msgBox.textContent = result.message
    if (200 <= res.status && res.status < 300) {
        loadMemo()
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
        // form.reset()
    } else {
        msgBox.classList.add('error')
        msgBox.classList.remove('success')
    }
    // document.querySelector('#contact-result').innerHTML = result;
    msgBox.textContent = result.message
})


// window.onload = function(){
//     let url = new URL(window.location.href);
//     for (let pair of url.searchParams.entries()) {
//         console.log(`key: ${pair[0]}, value: ${pair[1]}`)
//         const id = pair[1];
//         // vet_clinic_id =  pair[1];
//         loadDetail(vet_clinic_id)
//     }
    // const ids = url.searchParams.entries();
    // const pair = ids[0];
    // const id = pair[1];
// }