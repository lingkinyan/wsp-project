// import { createRequire } from 'module';
// const require = createRequire(import.meta.url);
// const moment = require('moment');
let box = document.querySelector('.boxGroup')
let totalBox = document.querySelector('.totalBox')
let memoContainer = document.querySelector(".boxGroup1")
let searchInput = document.querySelector(".search")
let searchDistrict = document.querySelector(".searchDistrict")

const dayOfWeekName = new Date().toLocaleString(
  'default', { weekday: 'long' }
);

function showMemo(memo) {
  let date = new Date().toDateString();
  let div = document.createElement('div')
  div.className = "boxGroup"
  div.id = memo.id;
  const monStartTime1 = memo.monday_start_time;
  const monEndTime1 = memo.monday_end_time;
  const monStartTime2 = memo.monday_start_time2;
  const monEndTime2 = memo.monday_end_time2;
  const tueStartTime1 = memo.tuesday_start_time;
  const tueEndTime1 = memo.tuesday_end_time;
  const tueStartTime2 = memo.tuesday_start_time2;
  const tueEndTime2 = memo.tuesday_end_time2;
  const wedStartTime1 = memo.wednesday_start_time;
  const wedEndTime1 = memo.wednesday_end_time;
  const wedStartTime2 = memo.wednesday_start_time2;
  const wedEndTime2 = memo.wednesday_end_time2;
  const thuStartTime1 = memo.thursday_start_time;
  const thuEndTime1 = memo.thursday_end_time;
  const thuStartTime2 = memo.thursday_start_time2;
  const thuEndTime2 = memo.thursday_end_time2;
  const friStartTime1 = memo.friday_start_time;
  const friEndTime1 = memo.friday_end_time;
  const friStartTime2 = memo.friday_start_time2;
  const friEndTime2 = memo.friday_end_time2;
  const satStartTime1 = memo.saturday_start_time;
  const satEndTime1 = memo.saturday_end_time;
  const satStartTime2 = memo.saturday_start_time2;
  const satEndTime2 = memo.saturday_end_time2;
  const sunStartTime1 = memo.sunday_start_time;
  const sunEndTime1 = memo.sunday_end_time;
  const sunStartTime2 = memo.sunday_start_time2;
  const sunEndTime2 = memo.sunday_end_time2;

  let businessHour = "";
  let isOperating = "";

  function defBusinessHour(st1, et1, st2, et2) {
    // rest day
    if ((st1 == "" || null || undefined) && (et1 == "" || null || undefined) && (st2 == "" || null || undefined) && (et2 == "" || null || undefined)) {
      businessHour = `今日休息`;
      return;
    }
    // 24 hours business
    if ((st1 == "00:00") && (et1 == "00:00")) {
      businessHour = `24 小時營業`;
      return;
    }
    // set up variables for comparing time
    // let currentTime = moment();
    // let opening = moment(st1, 'h:mma');
    // let closing = moment(et1, 'h:mma');
    // let opening2 = moment(st2, 'h:mma');
    // let closing2 = moment(et2, 'h:mma');

    // one time slot today
    if ((st2 == "" || null || undefined) && (et2 == "" || null || undefined)) {
      businessHour = `${st1}-${et1}`;
      // if (currentTime.isBetween(opening, closing)) {
      //   isOperating = `(營業中)`;
      // } else {
      //   isOperating = `(已打烊)`;
      // }
      return;
    }
    // two time slots today (usually splitted by a lunch break)
    businessHour = `${st1}-${et1}; ${st2}-${et2}`;
    // if ((currentTime.isBetween(opening, closing)) || (currentTime.isBetween(opening2, closing2))) {
    //   isOperating = `(營業中)`;
    // } else if (currentTime.isBetween(closing, opening2)) {
    //   isOperating = `(午休中)`
    // } else {
    //   isOperating = `(已打烊)`;
    // }
    return;
  }

  // show today's business hour (and operating status)
  if (dayOfWeekName == "Monday") {
    defBusinessHour(monStartTime1, monEndTime1, monStartTime2, monEndTime2);
  }
  if (dayOfWeekName == "Tuesday") {
    defBusinessHour(tueStartTime1, tueEndTime1, tueStartTime2, tueEndTime2);
  }
  if (dayOfWeekName == "Wednesday") {
    defBusinessHour(wedStartTime1, wedEndTime1, wedStartTime2, wedEndTime2);
  }
  if (dayOfWeekName == "Thursday") {
    defBusinessHour(thuStartTime1, thuEndTime1, thuStartTime2, thuEndTime2);
  }
  if (dayOfWeekName == "Friday") {
    defBusinessHour(friStartTime1, friEndTime1, friStartTime2, friEndTime2);
  }
  if (dayOfWeekName == "Saturday") {
    defBusinessHour(satStartTime1, satEndTime1, satStartTime2, satEndTime2);
  }
  if (dayOfWeekName == "Sunday") {
    defBusinessHour(sunStartTime1, sunEndTime1, sunStartTime2, sunEndTime2);
  }

  div.innerHTML = /* html */`

        <div class="upperBox">
          <img class="imgBox" src="${memo.photo ? "/clinicPhoto/" + memo.photo : "https://previews.123rf.com/images/milankivi/milankivi1612/milankivi161200059/69508538-veterinary-clinic-banner-with-dog-and-laboratory-on-background.jpg"}" alt="">
        </div>
        <div class="box">
          <h3>${memo.chinese_name}</h3>
        </div>
        <div class="box">
          <p>地址：${memo.chinese_address}</p>
        </div>
        <div class="box">
          <p>電話：${memo.phone}</p>    
        </div>
        <div class="box">
          <p>營業時間：${businessHour}</p>
        </div>
        `

  div.addEventListener("click", (event) => {
    window.location = `/comment.html?id=${event.currentTarget.id}`;
    console.log(event.currentTarget.id);
  })

  memoContainer.appendChild(div);

  searchInput.addEventListener('keyup', function () {
    let filter = searchInput.value
    console.log(filter)
    console.log(`${memo.name}`)
    memoContainer.appendChild(div);


    if (`${memo.english_name}`.includes(filter) ||
      `${memo.chinese_name}`.includes(filter)) {
      memoContainer.appendChild(div);
    } else {
      memoContainer.removeChild(div)
      if (memoContainer.children.length == "") {
        let noResult = document.querySelector('.noResult')
        noResult.innerHTML = `沒有搜尋結果`
        let footer = document.querySelector('.footer')
        footer.classList.add("position-fixed")
        footer.classList.add("bottom-0")
        footer.classList.add("w-100")
      } else {
        let noResult = document.querySelector('.noResult')
        noResult.innerHTML = ""
        let footer = document.querySelector('.footer')
        footer.classList.remove("position-fixed")
        footer.classList.remove("bottom-0")
        footer.classList.remove("w-100")
        footer.classList.remove("position-fixed")
        footer.classList.add("position-initial")
      }
    }
  })

  searchDistrict.addEventListener('keyup', function () {
    let filter = searchDistrict.value
    console.log(filter)
    console.log(`${memo.name}`)
    memoContainer.appendChild(div);

    if (`${memo.english_district}`.includes(filter) ||
      `${memo.chinese_address}`.includes(filter)) {
      memoContainer.appendChild(div);
    } else {
      memoContainer.removeChild(div)
      if (memoContainer.children.length == "") {
        let noResult = document.querySelector('.noResult')
        noResult.innerHTML = `沒有搜尋結果`
        let footer = document.querySelector('.footer')
        footer.classList.add("position-fixed")
        footer.classList.add("bottom-0")
        footer.classList.add("w-100")
      } else {
        let noResult = document.querySelector('.noResult')
        noResult.innerHTML = ""
        let footer = document.querySelector('.footer')
        footer.classList.remove("position-fixed")
        footer.classList.remove("bottom-0")
        footer.classList.remove("w-100")
        footer.classList.remove("position-fixed")
        footer.classList.add("position-initial")
      }
    }
  })
}

async function loadMemo() {
  let res = await fetch('/vet_clinic_all')
  let json = await res.json()
  //要清空返 如果唔係一加新野 舊果d又重覆loop一次
  memoContainer.innerHTML = ""

  json.reverse()
  for (let memo of json) {

    showMemo(memo)
    // console.log(memo)
  }
}

loadMemo();

// Setting up the map
var map = L.map('map').setView([22.35304, 114.062085], 15);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 20,
  minZoom: 13,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: 'sk.eyJ1IjoiYWxleHRzZTE3IiwiYSI6ImNreXRwcTBidzB5bHQybnFhMDdoZm9zOHYifQ.OOJ75ojaU8TV1WYvbBMKWQ'
}).addTo(map);

var lc = L.control.locate({
  position: 'topright',
  strings: {
    title: "Show me where I am, yo!"
  }
}).addTo(map);

async function loadMap() {

  var vetClinicIcon = L.icon({
    iconUrl: './assets/map_icon/map_vet.png',
    // shadowUrl: './icons/leaf-shadow.png',

    iconSize: [55, 55], // size of the icon
    // shadowSize: [50, 64], // size of the shadow
    iconAnchor: [0, 0], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
  });
  let res = await fetch('/vet_clinic_all');
  let vetClinicList = await res.json();

  let listIP = [];
  for (let vetClinic of vetClinicList) {
    const monStartTime1 = vetClinic.monday_start_time;
    const monEndTime1 = vetClinic.monday_end_time;
    const monStartTime2 = vetClinic.monday_start_time2;
    const monEndTime2 = vetClinic.monday_end_time2;
    const tueStartTime1 = vetClinic.tuesday_start_time;
    const tueEndTime1 = vetClinic.tuesday_end_time;
    const tueStartTime2 = vetClinic.tuesday_start_time2;
    const tueEndTime2 = vetClinic.tuesday_end_time2;
    const wedStartTime1 = vetClinic.wednesday_start_time;
    const wedEndTime1 = vetClinic.wednesday_end_time;
    const wedStartTime2 = vetClinic.wednesday_start_time2;
    const wedEndTime2 = vetClinic.wednesday_end_time2;
    const thuStartTime1 = vetClinic.thursday_start_time;
    const thuEndTime1 = vetClinic.thursday_end_time;
    const thuStartTime2 = vetClinic.thursday_start_time2;
    const thuEndTime2 = vetClinic.thursday_end_time2;
    const friStartTime1 = vetClinic.friday_start_time;
    const friEndTime1 = vetClinic.friday_end_time;
    const friStartTime2 = vetClinic.friday_start_time2;
    const friEndTime2 = vetClinic.friday_end_time2;
    const satStartTime1 = vetClinic.saturday_start_time;
    const satEndTime1 = vetClinic.saturday_end_time;
    const satStartTime2 = vetClinic.saturday_start_time2;
    const satEndTime2 = vetClinic.saturday_end_time2;
    const sunStartTime1 = vetClinic.sunday_start_time;
    const sunEndTime1 = vetClinic.sunday_end_time;
    const sunStartTime2 = vetClinic.sunday_start_time2;
    const sunEndTime2 = vetClinic.sunday_end_time2;

    let businessHour = "";
    let isOperating = "";

    function defBusinessHour(st1, et1, st2, et2) {
      // rest day
      if ((st1 == "" || null || undefined) && (et1 == "" || null || undefined) && (st2 == "" || null || undefined) && (et2 == "" || null || undefined)) {
        businessHour = `今日休息`;
        return;
      }
      // 24 hours business
      if ((st1 == "00:00") && (et1 == "00:00")) {
        businessHour = `24 小時營業`;
        return;
      }
      // set up variables for comparing time
      // let currentTime = moment();
      // let opening = moment(st1, 'h:mma');
      // let closing = moment(et1, 'h:mma');
      // let opening2 = moment(st2, 'h:mma');
      // let closing2 = moment(et2, 'h:mma');

      // one time slot today
      if ((st2 == "" || null || undefined) && (et2 == "" || null || undefined)) {
        businessHour = `${st1}-${et1}`;
        // if (currentTime.isBetween(opening, closing)) {
        //   isOperating = `(營業中)`;
        // } else {
        //   isOperating = `(已打烊)`;
        // }
        return;
      }
      // two time slots today (usually splitted by a lunch break)
      businessHour = `${st1}-${et1}; ${st2}-${et2}`;
      // if ((currentTime.isBetween(opening, closing)) || (currentTime.isBetween(opening2, closing2))) {
      //   isOperating = `(營業中)`;
      // } else if (currentTime.isBetween(closing, opening2)) {
      //   isOperating = `(午休中)`
      // } else {
      //   isOperating = `(已打烊)`;
      // }
      return;
    }

    // show today's business hour (and operating status)
    if (dayOfWeekName == "Monday") {
      defBusinessHour(monStartTime1, monEndTime1, monStartTime2, monEndTime2);
    }
    if (dayOfWeekName == "Tuesday") {
      defBusinessHour(tueStartTime1, tueEndTime1, tueStartTime2, tueEndTime2);
    }
    if (dayOfWeekName == "Wednesday") {
      defBusinessHour(wedStartTime1, wedEndTime1, wedStartTime2, wedEndTime2);
    }
    if (dayOfWeekName == "Thursday") {
      defBusinessHour(thuStartTime1, thuEndTime1, thuStartTime2, thuEndTime2);
    }
    if (dayOfWeekName == "Friday") {
      defBusinessHour(friStartTime1, friEndTime1, friStartTime2, friEndTime2);
    }
    if (dayOfWeekName == "Saturday") {
      defBusinessHour(satStartTime1, satEndTime1, satStartTime2, satEndTime2);
    }
    if (dayOfWeekName == "Sunday") {
      defBusinessHour(sunStartTime1, sunEndTime1, sunStartTime2, sunEndTime2);
    }


    const coordinates = vetClinic.IP;
    if (coordinates !== "undefined") {
      let arrayIP = [];
      if (listIP.indexOf(coordinates) !== -1) {
        // Change Geographical coordinates if the vet clinic's IP is duplicated
        console.log("same IP!");
        arrayIP = coordinates.split(',');
        let movedLatitude = (+arrayIP[0] + 0.001).toString();
        let movedLongitude = (+arrayIP[1] + 0.001).toString();
        arrayIP.push(movedLatitude, movedLongitude);
      } else {
        arrayIP = coordinates.split(',');
      }
      // console.log(arrayIP);
      // console.log("listIP:", listIP);
      listIP.push(arrayIP);
      // console.log(arrayIP);
      const vetClinicInfo =
        /* html */
        `${vetClinic.chinese_name}<br>
        ${vetClinic.chinese_address}<br>
        電話： ${vetClinic.phone}<br>
        營業時間：${businessHour}`;
      L.marker(arrayIP, { icon: vetClinicIcon })
        .addTo(map)
        .bindPopup(vetClinicInfo);
    }
  }
}
loadMap();