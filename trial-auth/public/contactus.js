let submitInfo = document.querySelector('#contactUsForm')

submitInfo.addEventListener('submit', async function (event) {
    event.preventDefault(); //停左submit功能 做下面的ajax
    console.log('do some AJAX pic')
    // Serialize the Form afterwards
    const formObject = {};
    const form = event.target;
    // (debug)new FormData入面有個(form) ->放埋字入去 
    // const formData = new FormData();
    // formData.append('topic1', form.topic.value)
    // formData.append('content', form.content.value)
    // formData.append('vet_clinic_id', id)

    formObject['name'] = form.name.value;
    formObject['email'] = form.email.value;
    formObject['phone'] = form.phone.value;
    formObject['content'] = form.content.value;

    // console.log('formObject :', formData);
    // let msgBox = form.querySelector('.msg-box')

    // formData.append('photo', form.photo.files[0])
    // formObject['secondInput'] = form.secondInput.value;
    // formObject['thirdInput'] = form.thirdInput.value;
    
    try {
        const res = await fetch('/contactInfo', {
            method: "POST",
            headers:{
                "Content-Type":"application/json"
            },
            body: JSON.stringify(formObject),
        });
        const json = await res.json();
        if (200 <= res.status && res.status < 300) {
            // loadMemo()
            loadContactUsStatus({ message: json.message, isSuccess: true })
            setTimeout(() => {
                window.location.href = "/search.html"
            }, 2000)
            // form.reset()
        } else {
            loadContactUsStatus({ message: json.message, isSuccess: false })
        }
    } catch (error) {
        loadContactUsStatus({ message: error.toString(), isSuccess: false })
    }
    // document.querySelector('#contact-result').innerHTML = result;
    // msgBox.textContent = result.message
})

function loadContactUsStatus({ message, isSuccess }) {
    let msgBox = document.querySelector('.msg-box')
    if (isSuccess) {
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
        // form.reset()
    } else {
        msgBox.classList.remove('success')
        msgBox.classList.add('error')
    }
    msgBox.textContent = message
}
