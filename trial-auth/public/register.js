console.log('register.js loaded');

let registerForm = document.querySelector('#register_form')
// Load vet clinic list into register page
const vetClinicList = document.querySelector(".vetClinicList")

function getVetClinicInfo (clinic) {
    let option = document.createElement('option');
    option.setAttribute("value", `${clinic.id}`);
    option.innerHTML = /* html */`${clinic.name}`;
    vetClinicList.appendChild(option);
}

async function loadVetClinicList () {
    let req = await fetch('/data/current/vetClinicList');
    let json = await req.json();
    vetClinicList.innerHTML = /* html */``;
    for (let clinic of json.clinicList) {
        getVetClinicInfo(clinic);
    }
}
loadVetClinicList();

// Show additional select list and form for vet clinic user
let checkForVetClinicUser = document.querySelector("#checkboxForVetClinicUser")
let selectListVetClinic = document.querySelector(".select-list-vet-clinic")
let vetClinicUserRegisterForm = document.querySelector(".vet-clinic-user-register-form")
checkForVetClinicUser.addEventListener('change', (event) => {
    checkForVetClinicUser.toggleAttribute('checked')
    event.preventDefault()
    console.log("checkbox is changed")
    if (checkForVetClinicUser.checked) {
        selectListVetClinic.style.visibility = "visible"
        selectListVetClinic.style.display = "block"
        console.log("show select list")
        if (selectListVetClinic.value == "other vet clinic") {
            vetClinicUserRegisterForm.style.visibility = "visible"
            vetClinicUserRegisterForm.style.display = "block"
            console.log("show form")
        }
    } else {
        selectListVetClinic.style.visibility = "hidden"
        selectListVetClinic.style.display = "none"
        vetClinicUserRegisterForm.style.visibility = "hidden"
        vetClinicUserRegisterForm.style.display = "none"
        console.log("hide select list and form")
    }
})

selectListVetClinic.addEventListener('change', (event) => {
    event.preventDefault()
    if (selectListVetClinic.value == 0) {
        vetClinicUserRegisterForm.style.visibility = "visible"
        vetClinicUserRegisterForm.style.display = "block"
        console.log("show form")
    } else {
        vetClinicUserRegisterForm.style.visibility = "hidden"
        vetClinicUserRegisterForm.style.display = "none"
        console.log("hide form")
    }
})

// Get register page
// async function loadRegisterPage() {
//     const req = await fetch('register', {
//         method: 'GET'
//     })
//     console.log(req.json())
// }

// Submit register form to server
registerForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    let registrationInfo = {}
    registrationInfo['username'] = registerForm.username.value
    registrationInfo['email'] = registerForm.email.value
    registrationInfo['password'] = registerForm.password.value
    registrationInfo['checkboxForVetClinicUser'] = registerForm.checkboxForVetClinicUser.checked
    registrationInfo['vetClinicID'] = registerForm.vetClinicID.value
    registrationInfo['vetClinicName'] = registerForm.vetClinicName.value
    registrationInfo['vetClinicAddress'] = registerForm.vetClinicAddress.value
    registrationInfo['vetClinicDistrict'] = registerForm.vetClinicDistrict.value
    registrationInfo['vetClinicPhone'] = registerForm.vetClinicPhone.value
    registrationInfo['vetClinicOpeningHour'] = registerForm.vetClinicOpeningHour.value
    registrationInfo['vetClinicClosingHour'] = registerForm.vetClinicClosingHour.value
    // // console.log(registrationInfo);
    // let response = await fetch('/user/register', {
    //     method: 'POST',
    //     headers: { 'Content-Type': 'application/json' },
    //     body: JSON.stringify(registrationInfo)
    // })
    // console.log(response)
    // // window.location.href("/user/login");
    // // let result = response.json()
    // // console.log(result);

    try {
        let res = await fetch('/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(registrationInfo),
        })
        let json = await res.json()
        if (200 <= res.status && res.status < 300) {
          loadRegisterStatus({ message: json.message, isSuccess: true })
        //   Redirect vet clinic user to postdata.html
        if (json.is_vet_clinic_user) {
            setTimeout(() => {
                window.location.href = "/postdata.html"
            }, 2000)
            return
        }
          setTimeout(() => {
              window.location.href = "/index.html"
          }, 2000)
        //   loadUser()
        //   reconnectSocket()
        } else {
          loadRegisterStatus({ message: json.message, isSuccess: false })
        }
      } catch (error) {
        loadRegisterStatus({ message: error.toString(), isSuccess: false })
      }
})

function loadRegisterStatus({ message, isSuccess }) {
    let msgBox = document.querySelector('.msg-box')
    if (isSuccess) {
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
        // form.reset()
    } else {
        msgBox.classList.remove('success')
        msgBox.classList.add('error')
    }
    msgBox.textContent = message
}
  
