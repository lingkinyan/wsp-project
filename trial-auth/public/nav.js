document.querySelector("nav").outerHTML = /* html */ `
<nav class="navbar navbar-expand-lg navbar-light bg-grey text-white">
          <div class="container-fluid">
              <a class="navbar-brand d-lg-none text-white" href="index.html">Pet Society 香港寵物網</a
        >
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <a class="nav-link" href="index.html">
            <i class="fas fa-paw"><h5>Pet Society</h5></i>
          </a>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap">
            <li class="nav-item">
              <a class="nav-link active navMainPage" aria-current="page" href="index.html">主頁</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/searchVetClinic.html">尋找醫生</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/petprofile.html">我的寵物</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/game-login.html">聊天室</a>
          </li>
            <li class="nav-item">
              <a class="nav-link" href="contactus.html">聯繫我們</a>
            </li>
             <!-- Reserve for dropdown menu --> 
          </ul>
          <ul class="navbar-nav me-2 mb-4 mb-lg-0 flex-wrap">
            
            <li class="nav-item">
              <div class="nav-link welcome"></div >
            </li>
            <li class="nav-item">
              <a class="nav-link login" href="login.html">登入</a>
            </li>
            <li class="nav-item">
              <a class="nav-link register" href="register.html">註冊</a>
            </li>
            <li class="nav-item dropdown navBarRight">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                我的帳號
              </a>
              <ul class="dropdown-menu " aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="myaccount.html">個人帳戶</a></li>
                <li><a class="dropdown-item" href="postdata.html">診所帳戶</a></li>
                <form action="/logout" method="post" id="logoutButton">
                  <li><button type="submit" class="dropdown-item">登出</button></li>
                </form>
              </ul>
            </li>
          </ul>
        </div>
    </div>
    </nav>
`;
