let dataContainer = document.querySelector(".myAccountInformation")
let myAccount = document.querySelector("#myAccount")
let pettype = document.querySelector(".pettype");

const searchParams = new URLSearchParams(location.search)
let id = searchParams.get('id')

// ${data.icon ? `<img src="usericons/${data.icon}">` : ""}
function showData(data) {
    let div = document.createElement('div')

    div.innerHTML = /*html*/ `
    <div>登入名稱：
        ${data.username}
    </div>
    <div>電子郵件：
        ${data.email}
    </div>
    `
    dataContainer.appendChild(div)
}

async function loadData() {

    let res = await fetch('/myAccount')
    let json = await res.json()
    //要清空返 如果唔係一加新野 舊果d又重覆loop一次
    dataContainer.innerHTML = ""
    json.reverse()
    for (let data of json) {
        showData(data)
        // console.log(data)
        if (data.icon == null) {
            myAccount.blah.src = `https://png.pngtree.com/png-vector/20190130/ourlarge/pngtree-simple-and-cute-cartoon-people-and-cute-pets-petfresh-png-image_592461.jpg`
            return
        }
        else {
            myAccount.blah.src = `/usericons/${data.icon}`;
        }
    }
}
loadData()

myAccount.addEventListener('submit', async function (event) {
    event.preventDefault(); //停左submit功能 做下面的ajax
    console.log('do some AJAX pic')
    // Serialize the Form afterwards
    const formObject = {};
    const form = event.target;
    // (debug)new FormData入面有個(form) ->放埋字入去 
    // const formData = new FormData(form);
    // formObject['username'] = form.username.value;
    // formObject['email'] = form.email.value;

    let msgBox = form.querySelector('.msg-box')
    const formData = new FormData();

    formData.append("username", form.username.value)
    formData.append("email", form.email.value)
    formData.append('icon', form.icon.files[0])

    const res = await fetch('/myAccountRevise', {
        method: "PUT",
        body: formData
    });

    const result = await res.json();
    msgBox.textContent = result.message
    if (200 <= res.status && res.status < 300) {
        loadData()
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
        form.reset()
    } else {
        msgBox.classList.add('error')
        msgBox.classList.remove('success')
    }
    // // document.querySelector('#contact-result').innerHTML = result;
    msgBox.textContent = result.message
})

let remove = document.querySelector(".remove");
let iconInput = document.querySelector(".iconInput");
let icon = document.querySelector(".icon");

//remove
remove.addEventListener("click", function () {
    iconInput.innerHTML = ` <input type='file' id="imgInp" name='icon' id='icon' />`;
    imgInp.onchange = (evt) => {
        const [file] = imgInp.files;
        if (file) {
            blah.src = URL.createObjectURL(file);
        }
        if (!file) {
            blah.src = "https://i.pinimg.com/originals/18/82/e0/1882e07aecdf7a3286a5013cdad5d0c0.png";
        }
    };
});

//review the icon
imgInp.onchange = (evt) => {
    const [file] = imgInp.files;
    if (file) {
        blah.src = URL.createObjectURL(file);
    }
};