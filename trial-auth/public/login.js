// Get login page
// async function loadLoginPage() {
//     const res = await fetch('login', {
//         method: 'GET'
//     })
//     console.log(req.json())
// }

// Submit login form to server
let loginForm = document.querySelector('#login_form')
loginForm.addEventListener('submit', async(event) => {
    event.preventDefault()
    let loginInfo = {}
    loginInfo['username'] = loginForm.username.value
    loginInfo['password'] = loginForm.password.value
        // console.log(loginInfo);
    try {
        let res = await fetch('/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginInfo)
        })
        let json = await res.json()
        if (200 <= res.status && res.status < 300) {
            loadLoginStatus({ message: json.message, isSuccess: true })
            setTimeout(() => {
                window.location.href = "/index.html"
            }, 1000)
        } else {
            loadLoginStatus({ message: json.message, isSuccess: false })
        }
    } catch (error) {
        loadLoginStatus({ message: error.toString(), isSuccess: false })
    }
})

function loadLoginStatus({ message, isSuccess }) {
    let msgBox = document.querySelector('.msg-box')
    if (isSuccess) {
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
    } else {
        msgBox.classList.remove('success')
        msgBox.classList.add('error')
    }
    msgBox.textContent = message
}