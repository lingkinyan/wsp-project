import { Request, Response } from "express";
// import path from "path";
// import { db } from "./db";
// import multer from "multer";
// import jsonfile from "jsonfile";
// import { loggedIn} from "./guard";
import {CommentService} from "../services/comment-service"

export class CommentController{
    constructor(private commentService : CommentService){}

    getIntro = async(req:Request,res:Response)=>{
        try{
            console.log('123')
            const id = req.query.id;
            let result = await this.commentService.getIntro(id as string)
            // console.log(result)
            res.json(result);  
        }catch(err){
            console.log('get comment error');
            res.statue(500).json({ success:false });
        }
    }

    getComment = async(req:Request,res:Response)=>{
        try{
            const id = req.params.id;
            console.log('vet_clinic_id:', id)
            let result = await this.commentService.getComment(id)
            // console.log(result)
            res.json(result);  
        }catch(err){
            console.log('get comment error');
            res.statue(500).json({ success:false });
        }
    }

    delete = async(req:Request,res:Response)=>{
        try{
            const id = req.params.id;
            await this.commentService.delete(parseInt(id))
            res.json({success:true})
        }catch(err){
            console.log('delete error');
            res.statue(500).json({ success:false });
        }
    }

    put = async(req:Request,res:Response)=>{
        try{
            console.log('request body:', req.body);
              // const topic = req.body.topic;
              const content = req.body.content;
              // const filename = req.file?.filename; 
              const id = req.body.comment_on_vet_clinic_id;

              await this.commentService.put(content,id)
              res.json({message : '已成功修改'})
        }
        catch(err){
            console.log('error')
            res.statue(500).json({success:false});
        }
    }

    post = async(req:Request,res:Response)=>{
        try{
            const topic = req.body.topic1;
            const id = req.body.vet_clinic_id;
            const content = req.body.content;
            const filename = req.file ? req.file.filename : "" ;
            const user = req.session.user?.id;
        
            console.log('info :', topic, id, content,filename ,user);
            
            //http://localhost:8080/<filename>
            if (!topic) {
              res.status(400).json({ message: "Missing Topic!" });
              // .end('missing content in req.body')
              // cleanup()
              return;
            }
            if (!content) {
              res.status(400).json({ message: "Missing Content!" });
              // .end('missing content in req.body')
              // cleanup()
              return;
            }

            if(user){
                await this.commentService.post(topic,id,content,filename,user)
                res.json({success:true})
            }
            else{
                res.status(400).json({success:false});
            }
        }
        catch(err){
            console.log('error')
            res.json({success:false})
        }
    }

    getReply = async(req:Request,res:Response)=>{
        try{
            const id = req.query.id;
            if(id){
                let result = await this.commentService.getReply(id as string)
                res.json(result)
            }
            else{
                res.json({ success:false })
            }
        }catch(err){
            console.log('post reply error');
            res.statue(500).json({ success:false });
            
        }
    }

    postReply = async(req:Request,res:Response)=>{
        try{
            const replyContent = req.body.replyContent;
            const comment_on_vet_clinic_id = req.body.comment_on_vet_clinic_id;
            const user = req.session.user?.id;

            if(user){
                await this.commentService.postReply(user,comment_on_vet_clinic_id,replyContent)
                res.json({success:true})
            }
            else{
                res.status(400).json({ message: "請先登入，以使用此功能" });
            }
        }catch(err){
            console.log('post reply error')
            res.json({success:false})
        }
    }

    likePost = async(req:Request,res:Response)=>{
        try{
            const user = req.session.user?.id
            const id = req.body.vet_clinic_id;
            if(user){
                await this.commentService.likePost(user,id)
                res.json({success:true})
            }else{
                res.status(400).json({ message: "請先登入，以使用此功能" });
            }
        }
        catch(err){
            console.log('like post error')
            res.json({success:false})
        }
    }

    likeGet = async(req:Request,res:Response)=>{
        try{
            const id = req.query.id
            const like = await this.commentService.likeGet(id as string)
            res.json(like) 
        }
        catch(err){
            console.log('like error');
            res.statue(500).json({ success:false });
            
        }
    }
}