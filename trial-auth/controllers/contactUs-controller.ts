import { Request, Response } from "express";
import { ContactUsService } from "../services/contactUs-service";

export class ContactUsController{
    constructor(private contactUsService:ContactUsService){}

    post = async(req:Request,res:Response)=>{
        
        const name: string = req.body.name
        const email:string = req.body.email
        const phone: Text = req.body.phone
        const content: Text = req.body.content

        if (!name || typeof name != "string") {
            res.status(400).json({ message: "請輸入姓名"});
            return
        }
        if (!email || typeof email != "string") {
            res.status(400).json({ message: "請輸入電郵地址"});
            return
        }
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
            res.status(400).json({ message: "請輸入有效電郵地址"});
            return
        }
        if (!phone || typeof phone != "string") {
            res.status(400).json({ message: "請輸入電話號碼"});
            return
        }
        if (!content || typeof content != "string") {
            res.status(400).json({ message: "請輸入內容"});
            return
        }
        try{
            await this.contactUsService.post(name,email,phone,content)
            res.status(200).json({ message: "感謝您的聯繫！我們會盡快回覆您:)"})
        }
        catch(err){
            console.log('error')
            res.status(500).json({ message: "falied"})
        }
    }
}
