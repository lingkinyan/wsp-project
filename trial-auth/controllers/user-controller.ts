import { Request, Response } from "express";
import { checkPassword, hashPassword } from "../hash";
import { UserService } from "../services/user-service";

export class UserController {
  constructor(private userService: UserService) {}

  loadVetClinicList = async (req: Request, res: Response) => {
    try {
      let clinicList = await this.userService.getVetClinicList();
      res.json({ clinicList });
    } catch (error: any) {
      console.error("Failed to get vet clinic list", error);
      res.status(500).json({ message: "Database Error" });
      return;
    }
  };

  register = async (req: Request, res: Response) => {
    const username: string = req.body.username;
    const email: string = req.body.email;
    const password: string = req.body.password;
    const isVetClinicUser: boolean = req.body.checkboxForVetClinicUser;
    let vetClinicID: any = req.body.vetClinicID;

    // Check the existence and appropriateness of type of username input
    if (!username || typeof username != "string") {
      res.status(400).json({ message: "請輸入用戶名稱" });
      return;
    }
    // Check the existence and appropriateness of type of email input
    if (!email || typeof email != "string") {
      res.status(400).json({ message: "請輸入電郵地址" });
      return;
    }
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      res.status(400).json({ message: "請輸入有效電郵地址" });
      return;
    }
    // Check the existence and appropriateness of type of password input
    if (!password || typeof password != "string") {
      res.status(400).json({ message: "請輸入密碼" });
      return;
    }

    // Registering as a normal user

    if (!isVetClinicUser) {
      try {

        let rows: any = await this.userService.checkUserByUsername(username);
        if (rows.length !== 0) {
          res.status(400).json({
            message: "此名稱已註冊，請輸入其他名稱。",
          });
          return;
        }
      
        rows = await this.userService.checkUserByEmail(email);
        if (rows.length !== 0) {
          res.status(400).json({
            message: "此電郵地址已註冊，請輸入其他電郵地址。",
          });
          return;
        }
      
        // Check validity of password
        if (!(password.length >= 8 && password.length <= 20)) {
          res.status(400).json({ message: "密碼應為 8-20 個字符" });
          return;
        }

        // Insert new user's information into DB
     
        const hashedPassword = await hashPassword(password);
        vetClinicID = null;
        await this.userService.insertNewUser(
          username,
          email,
          hashedPassword,
          vetClinicID
        );
        console.log("Register successfully!");
 
        // Obtain user's info in session
      
        rows = await this.userService.checkUserByUsernameForSessionSave(
          username
        );
        let user = rows[0];
        // Save user's info in session and ready to be redirected to index.html
        req.session.user = {
          id: user.id,
          username: username,
          is_vet_clinic_user: false,
          is_admin: false,
        };
        req.session.save();
        res.status(200).json({ message: "成功註冊！" });
      } catch (error: any) {
        console.error("Failed to get user for registration", error);
        res.status(500).json({ message: "Database Error" });
        return;
      }
     
    }
    // Registering as a vet clinic user
    else {
      // Check if the user selected any option except for default option
      if (vetClinicID == -1) {
        res.status(400).json({ message: "請選擇您的診所！" });
        return;
      }
      // Check if the vet clinic is registered
      // Vet clinic is registered
      if (vetClinicID > 0) {
        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkUserByUsername(username);
          // Obtain a user result using username
          if (rows.length !== 0) {
            res.status(400).json({
              message: "此名稱已註冊，請輸入其他名稱。",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkUserByEmail(email);
          // Obtain a user result using email
          if (rows.length !== 0) {
            res.status(400).json({
              message: "此電郵地址已註冊，請輸入其他電郵地址。",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Check validity of password
        if (!(password.length >= 8 && password.length <= 20)) {
          res.status(400).json({ message: "密碼應為 8-20 個字符" });
          return;
        }

        // Insert new user's information into DB
        try {
          const hashedPassword = await hashPassword(password);
          await this.userService.InsertNewUserWithVetClinic({
            username,
            email,
            hashedPassword,
            vetClinicID,
          });
          console.log("Register successfully!");
          // Obtain user's info in session
          try {
            let rows = await this.userService.checkUserByUsernameForSessionSave(
              username
            );
            let user = rows[0];
            // Save user's info in session and ready to be redirected to index.html
            req.session.user = {
              id: user.id,
              username: username,
              is_vet_clinic_user: true,
              is_admin: false,
            };
            req.session.save();
            res.status(200).json({ message: "成功註冊成為獸醫診所用戶！" });
          } catch (error: any) {
            console.error("Failed to get user for login", error);
            res.status(500).json({ message: "Database Error" });
            return;
          }
        } catch (error) {
          console.error("Cannot register due to database issues (Type1)");
          res.redirect("/register.html");
        }
      }
      // Vet clinic is not registered
      else {
        const vetClinicName: string = req.body.vetClinicName;
        const vetClinicAddress: string = req.body.vetClinicAddress;
        const vetClinicDistrict: string = req.body.vetClinicDistrict;
        const vetClinicPhone: string = req.body.vetClinicPhone;
        const vetClinicOpeningHour: string = req.body.vetClinicOpeningHour;
        const vetClinicClosingHour: string = req.body.vetClinicClosingHour;
        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkVetClinicByName(vetClinicName);
          // Obtain a result using name of registered vet clinic
          if (rows.length !== 0) {
            res.status(400).json({
              message:
                "此診所名稱已註冊。請輸入其他診所名稱，或在上方選擇您的診所。",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Check the existence and appropriateness of type of username input
        if (!vetClinicPhone || typeof vetClinicPhone != "string") {
          res.status(400).json({ message: "請輸入電話號碼" });
          return;
        }
        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkVetClinicByPhone(
            vetClinicPhone
          );
          // Obtain a result using phone of registered vet clinic
          if (rows.length !== 0) {
            res.status(400).json({
              message:
                "此電話號碼已註冊。如果您的電話號碼被佔用，請通過“聯繫我們”報告此問題，謝謝！",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkUserByUsername(username);
          // Obtain a user result using username
          if (rows.length !== 0) {
            res.status(400).json({
              message: "此名稱已註冊，請輸入其他名稱。",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Obtain above selected result and catch possible DB error
        try {
          let rows = await this.userService.checkUserByEmail(email);
          // Obtain a user result using email
          if (rows.length !== 0) {
            res.status(400).json({
              message: "此電郵地址已註冊，請輸入其他電郵地址。",
            });
            return;
          }
        } catch (error: any) {
          console.error("Failed to get user for registration", error);
          res.status(500).json({ message: "Database Error" });
          return;
        }

        // Check validity of password
        if (!(password.length >= 8 && password.length <= 20)) {
          res.status(400).json({ message: "密碼應為 8-20 個字符" });
          return;
        }

        // Insert new Vet Clinic's information into DB & Obtain ID of newly added Vet Clinic from DB
        try {
          vetClinicID = await this.userService.insertNewVetClinic(
            vetClinicName,
            vetClinicAddress,
            vetClinicDistrict,
            vetClinicPhone,
            vetClinicOpeningHour,
            vetClinicClosingHour
          );
          console.log("Register for vet clinic successfully!");
        } catch (error) {
          console.error(
            "Cannot register for vet clinic due to database issues (Type 2.1)"
          );
          res.redirect("/register.html");
        }

        // Insert new user's information into DB
        try {
          const hashedPassword = await hashPassword(password);
          await this.userService.insertNewUser(
            username,
            email,
            hashedPassword,
            vetClinicID
          );
          console.log("Register for vet clinic user successfully!");
          // Obtain user's info in session
          try {
            let rows = await this.userService.checkUserByUsernameForSessionSave(
              username
            );
            let user = rows[0];
            // Save user's info in session and ready to be redirected to postdata.html
            req.session.user = {
              id: user.id,
              username: username,
              is_vet_clinic_user: true,
              is_admin: false,
            };
            req.session.save();
            res.status(200).json({
              message: "成功註冊成為獸醫診所用戶！",
              is_vet_clinic_user: true,
            });
          } catch (error: any) {
            console.error("Failed to get user for login", error);
            res.status(500).json({ message: "Database Error" });
            return;
          }
        } catch (error) {
          console.error(
            "Cannot register for vet clinic user due to database issues (Type 2.2)"
          );
          res.redirect("/register.html");
        }
      }
    }
  };

  login = async (req: Request, res: Response) => {
    const username: string = req.body.username;
    const password: string = req.body.password;
    // console.log({ username, password });

    // Check the existence and appropriateness of type of username input
    if (!username || typeof username != "string") {
      res.status(400).json({ message: "請輸入用戶名稱" });
      return;
    }
    // Check the existence and appropriateness of type of password input
    if (!password || typeof password != "string") {
      res.status(400).json({ message: "請輸入密碼" });
      return;
    }

    // Obtain above selected result and catch possible DB error
    try {
      let rows = await this.userService.checkUserByUsernameForSessionSave(
        username
      );
      // Obtain no user result
      if (rows.length == 0) {
        res.status(400).json({
          message: "此用戶名稱未註冊",
        });
        return;
      }

      // Failed to match the hashed password
      let user = rows[0];
      if (!(await checkPassword(password, user.hashed_password))) {
        res.status(400).json({
          message: "用戶名或密碼不正確",
        });
        return;
      }

      req.session.user = {
        id: user.id,
        username: username,
        is_vet_clinic_user: user.is_vet_clinic_user,
        is_admin: user.is_admin,
      };
      req.session.save();
      res.json({ message: "登入成功！" });

    } 
    catch (error: any) {
      console.error("Failed to get user for login", error);
      res.status(500).json({ message: "Database Error" });
      return;

    }
  };

  logout = async (req: Request, res: Response) => {
    req.session.user = undefined;
    req.session.save();
    res.json({ message: "成功登出" });
  };
}
