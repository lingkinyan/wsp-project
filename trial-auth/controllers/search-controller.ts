import { Request, Response } from "express";

import { SearchService } from "../services/search-service";

export class SearchController {
  constructor(private searchService: SearchService) {}

  loadAllVetClinic = async (req: Request, res: Response) => {
    let result = await this.searchService.getAllVetClinic();
    res.json(result);
  };
}
