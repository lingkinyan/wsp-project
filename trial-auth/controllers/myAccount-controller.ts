import { Request, Response } from "express";
import { MyAccountService } from "../services/myAccount-service";



export class MyAccountController{
    constructor(private myAccountService : MyAccountService){}
    
    get = async(req:Request,res:Response)=>{
        const user_id = req.session.user?.id;

        if(user_id){
            let result = await this.myAccountService.get(user_id)
            res.status(200).json(result);
        }else{
            res.status(500).json('no user_id')
        }
    }

    put = async(req:Request,res:Response)=>{
        try{
            const reviseUserName = req.body.username
            const reviseUserEmail = req.body.email
            const filename = req.file ? req.file.filename : "" ;
            const id = req.session.user?.id


            if(reviseUserName == ""){
                res.status(400).json({message: 'Missing username'})
                return;
            }

            if(reviseUserEmail == ""){
                // add checking for user emal
                res.status(400).json({message: 'Missing email'})
                return;
            }

            if(id){
                await this.myAccountService.put(reviseUserName,reviseUserEmail,filename,id)
                res.json({message : '已成功修改'})
            }

        }catch(err){
            res.json({success:false})
        }
    }

}