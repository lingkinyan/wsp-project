import { Request, Response } from "express";
import { MedicalService } from "../services/medical-service";

export class MedicalController {
  constructor(private medicalService: MedicalService) {}

  dataInsert = async (req: Request, res: Response) => {
    try {

      let user_id = req.session.user!.id;
      let name = req.body.petname;
      let sex = req.body.sex;
      let type = req.body.pettype;
      let filename = req.file?.filename || null;
      let breed = req.body.petbreed;
      let birthday = req.body.petbday || null;
      let weight = parseInt(req.body.weight);
      let chipNumber = req.body.chip;
      let neutered = req.body.neutered;
      let vac = req.body.vac;
      let vacdate = req.body.vacdate;
      let sickdate = req.body.sickdate;
      let history = req.body.history;

      let vacList: { name: string; date: string }[] = [];

      if (typeof vac == "string" && typeof vacdate == "string") {
        vacList.push({ name: vac, date: vacdate });
      } 
      else if (Array.isArray(vac) && Array.isArray(vacdate)) {
        if (vac.length != vacdate.length) {
          res
            .status(400)
            .json({ success: false, error: "missing vac or vacdate" });
          return;
        }

        vac.forEach((name, i) => {
          vacList.push({ name, date: vacdate[i] });
        });

      } 
      else {
        res
          .status(400)
          .json({ success: false, error: "missing vac or vacdate" });
        return;
      }

      let medicalList: { name: string; date: string }[] = [];

      if (typeof history == "string" && typeof sickdate == "string") {
        medicalList.push({ name: history, date: sickdate });
      } else if (Array.isArray(history) && Array.isArray(sickdate)) {

        if (history.length != sickdate.length) {
          res
            .status(400)
            .json({ success: false, error: "missing vac or vacdate" });
          return;
        }

        history.forEach((name, i) => {
          medicalList.push({ name, date: sickdate[i] });
        });

      } 
      else {
        res
          .status(400)
          .json({ success: false, error: "missing vac or vacdate" });
        return;
      }

      let result = await this.medicalService.dataInsert(
        user_id,
        name,
        sex,
        type,
        filename,
        breed,
        birthday,
        weight,
        chipNumber,
        neutered,
        // vac,
        // vacdate,
        // sickdate,
        // history
        vacList,
        medicalList
      );

      console.log(result);
      res.json({petId: result});
    } 
    catch (error) {
      console.error("Failed to insert the data", error);
      res.status(500).json({ message: "Cant insert the database" });
      return;
    }
  };

  dataUpdate = async (req: Request, res: Response) => {
    try {
      let user_id = req.session.user?.id;
      let pet_id = req.params.id;
      let name = req.body.petname;
      let sex = req.body.sex;
      let type = req.body.pettype;
      let filename = req.file?.filename || null;
      let breed = req.body.petbreed;
      let birthday = req.body.petbday || null;
      let weight = parseInt(req.body.weight);
      let chipNumber = req.body.chip;
      let neutered = req.body.neutered;
      let vac = req.body.vac;
      let vacdate = req.body.vacdate;
      let sickdate = req.body.sickdate;
      let history = req.body.history;
      let deleteImage = req.body.delete_image;

      let vacList: { name: string; date: string }[] = [];
      if (typeof vac == "string" && typeof vacdate == "string") {
        vacList.push({ name: vac, date: vacdate });
      } else if (Array.isArray(vac) && Array.isArray(vacdate)) {
        if (vac.length != vacdate.length) {
          res
            .status(400)
            .json({ success: false, error: "missing vac or vacdate" });
          return;
        }
        vac.forEach((name, i) => {
          vacList.push({ name, date: vacdate[i] });
        });
      } else {
        res
          .status(400)
          .json({ success: false, error: "missing vac or vacdate" });
        return;
      }

      let medicalList: { name: string; date: string }[] = [];
      if (typeof history == "string" && typeof sickdate == "string") {
        medicalList.push({ name: history, date: sickdate });
      } 
      else if (Array.isArray(history) && Array.isArray(sickdate)) {

        if (history.length != sickdate.length) {
          res
            .status(400)
            .json({ success: false, error: "missing vac or vacdate" });
          return;
        }

        history.forEach((name, i) => {
          medicalList.push({ name, date: sickdate[i] });
        });

      } 
      else {
        res
          .status(400)
          .json({ success: false, error: "missing vac or vacdate" });
        return;
      }

      let result = await this.medicalService.dataUpdate(
        user_id,
        pet_id,
        name,
        sex,
        type,
        filename,
        breed,
        birthday,
        weight,
        chipNumber,
        neutered,
        deleteImage,
        vacList,
        medicalList
        );

      console.log(result);
      res.json(result);

    } 
    catch (error) {
      console.error("Failed to update the data", error);
      res.status(500).json({ message: "Cant update the database" });
      return;
    }
  };

  loadData = async (req: Request, res: Response) => {
    try {
      const user_id = req.session.user!.id;
      const pet_id = req.params.id;

      if (!pet_id) {
        res.status(404).json({ error: "Missing pet id." });
        return;
      }

      let pet = await this.medicalService.loadData(user_id, pet_id);

      if(pet.length === 0){
        res.status(404).json({ error: "Pet not found" });
      }

      let medicalList = await this.medicalService.loadMedical(pet_id);

      if(medicalList.length === 0){
        res.status(404).json({ error: "Pet medicalList not found" });
      }

      let vacList = await this.medicalService.loadVac(pet_id);

      if(vacList.length === 0){
        res.status(404).json({ error: "Pet vacList not found" });
      }


      console.log("pet info loaded:", pet);
      res.json({ pet, medicalList, vacList});

    } 
    catch (error) {
      console.error("Failed to load the data", error);
      res.status(500).json({ message: "Cant load the database" });
      return;
    }
  };

}
