import { PetProfileService } from "../services/petprofile-service";
import { Request, Response } from "express";

export class PetProfileController {
  constructor(private petProfileService: PetProfileService) {}

  loadPetInfo = async (
    req: Request,
    res: Response
  ) => {
    const user_id = req.session.user!.id;

    try {
      let petList = await this.petProfileService.getPetInfo(user_id);
      res.json(petList);
    } 
    catch (error: any) {
      res.status(500).json({ error: error.toString() });
    }
  };
}
