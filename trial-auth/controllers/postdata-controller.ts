import { Request, Response } from "express";
import { PostDataService } from "../services/postdata-service";

export class PostDataController {
  constructor(private postDataService: PostDataService) {}

  getVetClinicInfo = async (req: Request, res: Response) => {
    const user_id = req.session.user!.id;
    try {
      let vetClinicID = await this.postDataService.getVetClinicID(user_id);
      if (vetClinicID.length <= 0) {
        res.status(404).json({ error: "Vet clinic ID not found" });
        return;
      }

      let vetClinicInfo = await this.postDataService.getVetClinicInfo(
        vetClinicID
      );
      res.json({ vetClinicInfo });
    } 
    catch (error: any) {
      res.status(500).json({ error: error.toString() });
    }
  };

  updateVetClinicInfo = async (req: Request, res: Response) => {
    const user_id = req.session.user!.id;
    const {
      name,
      address,
      district,
      phone,
      openingHour,
      closingHour,
      details,
      reservation,
    } = req.body;
    const photo = req.file!.filename;

    if (
      name == "" ||
      address == "" ||
      district == "" ||
      phone == "" ||
      openingHour == "" ||
      closingHour == ""
    ) {
      res.status(400).json({ message: "請重新檢查相關資料!" });
      return;
    }

    try {
      let vetClinicID = await this.postDataService.getVetClinicID(user_id);
      if (!vetClinicID) {
        res.status(404).json({ error: "Vet clinic ID not found" });
        return;
      }
      await this.postDataService.updateVetClinicInfo(
        vetClinicID,
        name,
        address,
        district,
        photo,
        phone,
        openingHour,
        closingHour,
        details,
        reservation
      );
      res.status(200).json({ message: "成功更新！" });
    } catch (error) {
      res.status(500).json({ message: "無法更新資料" });
    }
  };
}
