import { Application } from "express";
import { CommentController } from "./controllers/comment-controller";

export type Vet_clinic = {
    id: number
    name: string
    address: string
    district: string
    photo?: string
    phone: string
    opening_hour: string
    closing_hour: string
    details?: string
    reservation?: boolean
    created_at: Date
    updated_at: Date
}

export type User = {
    id: number
    username: string
    hashed_password: string
    email: string
    icon?: string
    address?: string
    is_vet_clinic_user: boolean
    vet_clinic_id?: number
    is_admin: boolean
    created_at: Date
    updated_at: Date
}    

export type Pet = {
    id: number
    user_id: number
    name: string
    icon?: string
    type: string
    breed: string
    sex: string
    birthday?: Date
    neutered?: boolean
    weight?: number
    chip_number?: string
    created_at: Date
    updated_at: Date
}    

export type Vet = {
    id: number
    name: string
    photo?: string
    vet_clinic_id: number
    created_at: Date
    updated_at: Date
}

export type Medical_history = {
    id: number
    pet_id: number
    date: Date
    name: string
    type?: string
    photo?: string
    details?: string
    vet_id?: number
    created_at: Date
    updated_at: Date
}        

export type CommentOnVetClinic = {
    id: number
    user_id: number
    vet_clinic_id: number
    topic: string
    date: Date
    content: string
    photo?: string
    created_at: Date
    updated_at: Date
}

export type VaccinationRecord = {
    id: number
    pet_id: number
    date: Date
    name: string
    created_at: Date
    updated_at: Date
}

export type ReplyOnComment = {
    id: number
    user_id: number
    comment_on_vet_clinic_id: number
    content: string
    created_at: Date
    updated_at: Date
}

export type LikesOnVetClinic = {
    id: number
    user_id: number
    comment_on_vet_clinic_id: number
    created_at: Date
}

export type ContactUsInfo = {
    id: number
    name: string
    email: string
    content: string
    created_at: Date
}

export interface RouterOptions {
    app: Application;
    commentController: CommentController;

}