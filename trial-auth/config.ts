import { config } from "dotenv";

config();

export let port = +process.env.PORT! || 8080 || 5432;
export let host = +process.env.HOST! || "192.168.1.129";
