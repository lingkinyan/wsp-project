import express from "express";
import path from "path";
// import { db } from "../db";
import multer from "multer";
// import jsonfile from "jsonfile";
import { loggedIn} from "../guard";
import { commentController } from "../server";

//console.log(commentController.post)
// import { CommentController } from "./controllers/comment-controller";
// import {multerSingleImage} from "./multer"

// fileCounter 用黎怕撞名 所以要++
let fileCounter = 0;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    fileCounter++;
    //呢部份係處理d照片格式名有時係有（- ；)
    let ext: string = file.mimetype.split("/").pop()!;
    ext = ext?.split("-").pop()!;
    ext = ext.split(";")[0];
    //利用callback to throw error
    if (ext != "jpeg" && ext != "png" && ext != "tiff" && ext !="jpg") {
      cb(new Error("Invalid photo"), null as any);
      //利用req.res令句error 靚仔少少
      req.res?.status(400).end("Invalid photo, only allow png format");
    } else {
      cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`);
    }
  },
});

// let memoList: any = [];

const upload = multer({ storage });

export const commentRoutes = express.Router();

commentRoutes.get("/comment_on_vet_clinic/:id", commentController.getComment)
commentRoutes.delete("/comment_on_vet_clinic/:id",loggedIn,commentController.delete)
commentRoutes.put("/comment_on_vet_clinic/:id",loggedIn,upload.none(),commentController.put)
commentRoutes.post("/comment_on_vet_clinic",loggedIn,upload.single("photo"),commentController.post)
commentRoutes.get("/reply_on_comment", commentController.getReply)
commentRoutes.post("/reply_on_vet_clinic",loggedIn, upload.single("photo"),commentController.postReply)
commentRoutes.post("/like", commentController.likePost)
commentRoutes.get("/like", commentController.likeGet)
commentRoutes.get("/vet_clinic", commentController.getIntro)

//only for get JSON file(can delete)
// commentRoutes.get("/memo", async (req, res) => {
//   const users = await jsonfile.readFile(path.join(__dirname, "users.json"));
//   res.json(users);
// });


//main get on comment
// commentRoutes.get("/comment_on_vet_clinic/:id", async(req,res) =>{
//   const id = req.params.id;
//   console.log('vet_clinic_id:', id)

//   const comment = await db.query(
//     `
//     select comment_on_vet_clinic.* ,
//     "user".icon,
//     "user".username,
//     json_agg( 
//       json_build_object(
//         'comment', reply_on_comment.content,
//         'update',reply_on_comment.created_at, 
//         'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id) 
//       ) 
//     ) as comment
//     from comment_on_vet_clinic
//     join "user"
//     on comment_on_vet_clinic.user_id  = "user".id
//     LEFT JOIN reply_on_comment 
//     ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
//     where comment_on_vet_clinic.vet_clinic_id = $1
//     GROUP BY comment_on_vet_clinic.id, "user".icon, "user".username;
//     `,[id]);


//   res.json(comment.rows)
// })

//delete comment
// commentRoutes.delete("/comment_on_vet_clinic/:id",loggedIn,async(req,res)=>{
//   const id = req.params.id;
//   await db.query(`delete from comment_on_vet_clinic where id=${id}`)
//   res.json({success:true})
// })

//revise comment
// commentRoutes.put("/comment_on_vet_clinic/:id",loggedIn,upload.none(),async(req,res)=>{
//   try{
//     console.log('request body:', req.body);
//       // const topic = req.body.topic;
//       const content = req.body.content;
//       // const filename = req.file?.filename; 
//       const id = req.body.comment_on_vet_clinic_id;
//       console.log(content)
//       console.log(id)
//       await db.query(`UPDATE comment_on_vet_clinic SET content = $1 where id =$2`,
//       [content,id]);
//       res.json({message : '已成功修改'})
//   }catch(err){
//       console.log('error')
//       res.json({success:false});
//   }
// })

//post content form database
// commentRoutes.post("/comment_on_vet_clinic",loggedIn,upload.single("photo"),async (req, res) => {
//   console.log("req.body:", req.body);
//   console.log("req.file:", req.file);
//   //input to database
//   try{
//     const topic = req.body.topic1;
//     const id = req.body.vet_clinic_id;
//     const content = req.body.content;
//     const filename = req.file?.filename;
//     const user = req.session.user?.id

//     console.log('info :', topic, id, content,filename ,user);
    
//     //http://localhost:8080/<filename>
//     if (!topic) {
//       res.status(400).json({ message: "Missing Topic!" });
//       // .end('missing content in req.body')
//       // cleanup()
//       return;
//     }
//     if (!content) {
//       res.status(400).json({ message: "Missing Content!" });
//       // .end('missing content in req.body')
//       // cleanup()
//       return;
//     }
//     if(filename){
//         // await db.query(`INSERT into comment_on_vet_clinic (user_id,vet_clinic_id,topic,date,content,photo,created_at,updated_at) values 
//         //                                                       (1,1,'${topic}',CURRENT_DATE,'${content}','${filename}',now(),now())`)
//         await db.query(`INSERT into comment_on_vet_clinic (user_id,vet_clinic_id,topic,date,content,photo,created_at,updated_at) values 
//         ($1,$2,$3,CURRENT_DATE,$4,$5,now(),now())`,
//         [user, id, topic,content,filename])
//         console.log(filename)
//     }else{
//         // await db.query(`INSERT into comment_on_vet_clinic (user_id,vet_clinic_id,topic,date,content,created_at,updated_at) values 
//         // (1,1,'${topic}',CURRENT_DATE,'${content}',now(),now())`)
//         await db.query(`INSERT into comment_on_vet_clinic (user_id,vet_clinic_id,topic,date,content,created_at,updated_at) values 
//         ($1,$2,$3,CURRENT_DATE,$4,now(),now())`,
//         [user,id,topic,content])
//     }
//     res.json({success:true})
// }
// catch(ex){
//     console.log('error')
//     res.json({success:false})
// }
// });

// commentRoutes.get("/reply_on_comment",async(req,res)=>{
//   let id = req.query.id;
//   const memos = await db.query(`select * from vet_clinic where id=${id}`);
//   res.json(memos.rows);
// });


//reply
// commentRoutes.post("/reply_on_vet_clinic",loggedIn, upload.single("photo"),async (req, res) => {
//   console.log("body:", req.body);
//   console.log("body:", req.file);
//   console.log("date", req.body.created_at)
//   //input to database
//   try{
//     const replyContent = req.body.replyContent
//     const comment_on_vet_clinic_id = req.body.comment_on_vet_clinic_id
//     const user = req.session.user?.id
//     // let date = JSON.stringify(new Date())
//     // let date1 = date.slice(0,5)
//     // let date = new Date();
//     // const id = req.body.id;

//     await db.query(`insert into reply_on_comment 
//     (user_id,comment_on_vet_clinic_id,content,created_at,updated_at) values 
//     ($1,$2,$3,now(),now())`,
//     [user,comment_on_vet_clinic_id,replyContent])
//     res.json({success:true})
// }
// catch(ex){
//     console.log('error')
//     res.json({success:false})
// }
// });



// commentRoutes.get("/vet_clinic",async(req,res)=>{
//   let id = req.query.id;
//   const memos = await db.query(`select * from vet_clinic where id=${id}`);
//   res.json(memos.rows);
// });

// commentRoutes.get("/vet_clinic/:id",async(req,res)=>{
//   const id = req.params.id;
//   await db.query(`select from vet_clinic where id=${id}`)
//   res.json({success:true})
// })

// commentRoutes.get("/comment_on_vet_clinic/:id",async(req,res)=>{
//   const id = req.params.id;
//   await db.query(`select from comment_on_vet_clinic where id=${id}`)
//   res.json({success:true})
// })


//like 
// commentRoutes.post("/like",loggedIn,async (req, res) => {
//   // console.log("req.body:", req.body);
//   // console.log("req.file:", req.file);
//   //input to database
//   try{
//     const user = req.session.user?.id
//     const id = req.body.vet_clinic_id;

//     console.log('info :' ,id , user);
//     let db_likes = await db.query(`select * from likes_on_vet_clinic where user_id=$1 and vet_clinic_id=$2;`, [user, id])
//     console.log('Like from DB', db_likes.rowCount);

//         if(db_likes.rowCount == 0){
//           await db.query(`INSERT into likes_on_vet_clinic (user_id,vet_clinic_id,created_at) values 
//           ($1,$2,now())`,
//           [user,id])  
//         }
    
//     res.json({success:true})
// }
// catch(ex){
//     console.log('error')
//     res.json({success:false})
// }
// })

// commentRoutes.get("/like",async(req,res)=>{
//   const id = req.query.id
//   const like = await db.query(`select user_id from likes_on_vet_clinic where vet_clinic_id = ${id}`)
//   res.json(like.rows)
//   // console.log('id', id);
//   // console.log(like.rows);
// })
