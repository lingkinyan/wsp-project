import express from "express";
import multer from "multer";
// import { db } from "../db";
import { loggedInOrRedirect} from "../guard";
import "../session";
import path from "path";
import { myAccountController } from "../server";


export const myAccountRoutes = express.Router();

let fileCounter = 0;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./usericons"));
  },
  filename: function (req, file, cb) {
    fileCounter++;
    //呢部份係處理d照片格式名有時係有（- ；)
    let ext: string = file.mimetype.split("/").pop()!;
    ext = ext?.split("-").pop()!;
    ext = ext.split(";")[0];
    //利用callback to throw error
    if (ext != "jpeg" && ext != "png" && ext != "tiff" && ext !="jpg") {
      cb(new Error("Invalid photo"), null as any);
      //利用req.res令句error 靚仔少少
      req.res?.status(400).end("Invalid photo, only allow png format");
    } else {
      cb(null, `${file.fieldname}-${Date.now()}-${fileCounter}.${ext}`);
    }
  },
});

const icon = multer({ storage: storage });

myAccountRoutes.get("/myAccount",loggedInOrRedirect, myAccountController.get)
myAccountRoutes.put('/myAccountRevise',loggedInOrRedirect,icon.single('icon'), myAccountController.put)




// myAccountRoutes.get('/myAccount',loggedInOrRedirect,async function(req,res){    
//     const user_id = req.session.user?.id;
  
//     const memos = await db.query(`select * from "user" where id = $1`,[user_id]);
//     res.json(memos.rows)
//   })

// myAccountRoutes.put('/myAccountRevise',loggedInOrRedirect,icon.single('icon'),async (req,res) =>{
//     try{
//     const reviseUserName = req.body.username
//     const reviseUserEmail = req.body.email
//     const filename = req.file?.filename;
//     const id = req.session.user?.id

//     const putWithFile = await db.query(`UPDATE "user" SET username = $1, email = $2, icon = $3 where id = $4`,
//     [reviseUserName,reviseUserEmail,filename,id])

//     const putWithoutFile = await db.query(`UPDATE "user" SET username = $1, email = $2 where id = $3`,
//     [reviseUserName,reviseUserEmail,id])
    
//     if(reviseUserName == ""){
//         res.status(400).json({message: 'Missing username'})
//         return;
//     }
//     if(reviseUserEmail == ""){
//         res.status(400).json({message: 'Missing email'})
//         return;
//     }
//     if(filename){
//         putWithFile
//     }
//     if(!filename){
//         putWithoutFile
//     }
//     res.json({success:true})
//     }catch(err){
//         res.json({success:false})
//     }
// })