import express from "express";
// import { db } from "../db";
import { searchController } from "../server";
export const searchRoutes = express.Router();

searchRoutes.use(express.urlencoded({ extended: false }));
searchRoutes.use(express.json());

// searchRoutes.get("/vet_clinic", searchController.get)
searchRoutes.get("/vet_clinic_all", searchController.loadAllVetClinic);

// searchRoutes.get("/vet_clinic",async(req,res)=>{
//   let id = req.query.id;
//   const memos = await db.query(`select * from vet_clinic where id=${id}`);
//   res.json(memos.rows);
// });

// //use for loop search.html
// searchRoutes.get("/vet_clinic_all",async(req,res)=>{
//   const memos = await db.query(`select * from vet_clinic `);
//   res.json(memos.rows);
// });
