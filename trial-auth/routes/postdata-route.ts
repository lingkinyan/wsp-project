import express from "express";
import multer from "multer";
import path from "path";
import { vetClinicUserOnly } from "../guard";
import { postDataController } from "../server";

export const postDataRoutes = express.Router();

let fileCounter = 0;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./clinicPhoto"));
  },
  filename: function (req, file, cb) {
    fileCounter++;
    let timestamp = Date.now();
    let ext: string = file.mimetype.split("/").pop()!;
    ext = ext.split("-").pop()!;
    ext = ext.split(";")[0];
    if (ext != "jpeg" && ext != "jfif" && ext != "png") {
      cb(new Error("Invalid photo"), null as any);
      req.res
        ?.status(400)
        .end("Invalid photo, only allow jpeg, jfif and png formats");
    } else {
      cb(null, `${file.fieldname}-${timestamp}-${fileCounter}.${ext}`);
    }
  },
});

const clinicPhoto = multer({ storage: storage });

// Get vet clinic's current data
postDataRoutes.get(
  "/data/current/vetclinic",
  vetClinicUserOnly,
  postDataController.getVetClinicInfo
);

postDataRoutes.put(
  "/postdata.html",
  vetClinicUserOnly,
  clinicPhoto.single("photo"),
  postDataController.updateVetClinicInfo
);
