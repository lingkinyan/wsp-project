import express, {} from "express";
import dotenv from "dotenv";
import { contactUsController } from "../server";
// import { db } from "./db";

dotenv.config();

export const contactRoutes = express.Router();

contactRoutes.post("/contactInfo",contactUsController.post)

// contactRoutes.post("/contactInfo",async (req,res)=>{
//     const name: string = req.body.name
//     const email:string = req.body.email
//     const phone: Text = req.body.phone
//     const content: Text = req.body.content
//     console.log(req.body)

//     if (!name || typeof name != "string") {
//         res.status(400).json({ message: "請輸入姓名"});
//         return
//     }
//     if (!email || typeof email != "string") {
//         res.status(400).json({ message: "請輸入電郵地址"});
//         return
//     }
//     if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))) {
//         res.status(400).json({ message: "請輸入有效電郵地址"});
//         return
//     }
//     if (!phone || typeof phone != "string") {
//         res.status(400).json({ message: "請輸入電話號碼"});
//         return
//     }
//     if (!content || typeof content != "string") {
//         res.status(400).json({ message: "請輸入內容"});
//         return
//     }

//     try{
//         await db.query(`insert into contact_us_info(name,email,content,created_at,phone) values
//         ($1,$2,$3,now(),$4)`,[name, email, content, phone])
//     }catch(err){
//         console.log('error')
//     }
//     res.status(200).json({ message: "感謝您的聯繫！我們會盡快回覆您:)"})
// })