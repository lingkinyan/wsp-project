import express from "express";
import { loggedInOrRedirect } from "../guard";
import { petProfileController } from "../server";

export const profileRoutes = express.Router();

profileRoutes.get("/petprofile.html", loggedInOrRedirect);

profileRoutes.get(
  "/petprofile",
  loggedInOrRedirect,
  petProfileController.loadPetInfo
);
