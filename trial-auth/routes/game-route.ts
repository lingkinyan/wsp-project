import express, { Request, Response } from "express";
import expressSession from "express-session";
import { io } from "../server";
// import http from "http";
// import { Server as SocketIO } from "socket.io";
// import MessageStore from "./messageStore";

export const gameRoutes = express.Router();


gameRoutes.use(express.urlencoded({ extended: true }));
gameRoutes.use(express.json());

// const server = new http.Server(app);
// const io = new SocketIO(server);
//session
const sessionMiddleware = expressSession({
  secret: "Tecky Academy teaches typescript",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false },
});

gameRoutes.use(sessionMiddleware);

let playCurrentTime = 30;

//login
gameRoutes.post("/game-login", (req, res ) => {
  const { playerName } = req.query;
  if (playerName) {
    req.session["username"] = playerName;
    res.end(`Success`);
  } 
  else {
    res.status(400).end("Invalid Name");
  }
});

gameRoutes.post("/logout", (req, res, next) => {
  delete req.session["username"];
  res.redirect("/game-login.html");
});

gameRoutes.post("/simplePost", (req: Request, res: Response) => {
  io.emit("new-msg", { message: "normal message" });
});

gameRoutes.post("/to-public-group", (req: Request, res: Response) => {
  io.to("public-room").emit("new-message", {
    message: "message from public room",
  });

  res.json({ updated: 1 });
});

gameRoutes.post("/private_message", (req, res) => {
    let toPlayer = req.body.toPlayer;
    let sender = req.body.sender;

    console.log("toplayer = ", toPlayer);
    console.log("sender = ", sender);

    let initPrivateMessage = findPrivateMessages(toPlayer.id, sender.id);
    console.log("initPrivateMessage = ", initPrivateMessage);
    res.json(initPrivateMessage);
});

// messageStore
let messages:Array<Message>= []

type Message = {
    content: string;
    from: any;
    to: any;
  };

  export function findPrivateMessages(userID_first: string, userID_second: string) {
    return messages.filter(
      ({ from, to }) =>
        ((from.id as string) === userID_first &&
          (to.id as string) === userID_second) ||
        ((from.id as string) === userID_second &&
          (to.id as string) === userID_first)
    );
  }

export function saveMessage(message: Message) {
    messages.push(message);
  }

export function findMessagesForUser(userID: string) {
    return messages.filter(
      ({ from, to }) =>
        (from.id as string) === userID || (to.id as string) === userID
    );
  }

// let messageStore = new MessageStore();
let players: any = [];

io.on("connection", (socket) => {
  console.log(`User : ${socket.id} is on the server`);

  socket.on("join-public-room", (username) => {
    console.log(`${socket.id} join the room`);
    let id = `${socket.id}`
    socket.join("public-room")
    io.to("public-room").emit('connectToRoom',({username,id}))
    ;
  });

  socket.on("leave-room",(username)=>{
    socket.leave("public-room")
    io.to("public-room").emit('leaveTheRoom',(username))
  })

  socket.on("client_connect", (player) => {

    let playerInfo = {
      id: socket.id,
      username: player,
      score : 0
    };

    players.push(playerInfo);
    console.log(`${player}:${socket.id} has joined the room`);

    io.emit("allPlayerList", players);
    io.emit("new_player", playerInfo);

    console.log("new_player", playerInfo);

    console.log("username:", player);
    console.log("online list", players);

    socket.on("disconnect", () => {
      console.log(`User: ${player} has left`);
      let activeUser = findUserID(socket.id);
      players = players.filter((player: any) => player != activeUser);
      console.log("online list", players);
      io.emit("player_left", activeUser);
    });

  });

  function findUserID(socketID: string) {
    return players.find((player: any) => player.id == socketID);
  }

  socket.on("public_message",(data) => {
    io.emit(
        "new-msg-public",
        data
    );
  })

  socket.on("group_message",(data) => {
    io.to("public-room")
    .emit("new-msg-group",data)
  })

    function getUserBySocketID(socketID: string) {
        return players.find((player: any) => player.id == socketID);
    }

  socket.on("private_message", ({content,to}) => {
      console.log("content,to",{content,to})
      console.log("players", players)
        socket.to(to.id).emit("new-msg-private", {
        content,
        from: getUserBySocketID(socket.id),
    });
    //    console.log("content", data)
    //    io.to("public-room").emit("new-msg", data)
    let message = {
        from: getUserBySocketID(socket.id),
        to: getUserBySocketID(to.id),
        content,
    };
    console.log("message",message)

    saveMessage(message);
    let testFindResult = findMessagesForUser(to.id);
    console.log("testFindResult = ", testFindResult);
  });

  socket.on("photo", (data) => {
    console.log('photo ', data);
    io.emit("photo1", data);
  });
});

gameRoutes.use(express.static("public"));
gameRoutes.use(isLogin, express.static("protected"));

function isLogin(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  if (req.session["user"]) {
    next();
  } else {
    res.redirect("/login.html");
  }
}


