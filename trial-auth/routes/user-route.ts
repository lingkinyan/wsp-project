import "../session";
import { userController } from "../server";
import express from "express";

export const userRoutes = express.Router();

userRoutes.get("/data/current/vetClinicList", userController.loadVetClinicList);
userRoutes.post("/register", userController.register);

userRoutes.post("/login", userController.login);
userRoutes.post("/logout", userController.logout);

// async function loadVetClinicList (req: Request, res: Response) {
//   // Select fields of Vet Clinic User from DB
//   let clinicList: Pick<Vet_clinic, "id" | "name">[];
//   // Obtain above selected result and catch possible DB error
//   try {
//     let result = await db.query('select id, name from vet_clinic',);
//     clinicList = result.rows;
//     res.json({ clinicList });
//   } catch (error: any) {
//     console.error("Failed to get vet clinic list", error);
//     res.status(500).json({ message: "Database Error" });
//     return;
//   };
// };

// async function register(req: Request, res: Response) {
//   const username: string = req.body.username;
//   const email: string = req.body.email;
//   const password: string = req.body.password;
//   const isVetClinicUser: boolean = req.body.checkboxForVetClinicUser;
//   let vetClinicID: number = req.body.vetClinicID;

//   // Check the existence and appropriateness of type of username input
//   if (!username || typeof username != "string") {
//     res.status(400).json({ message: "請輸入用戶名稱" });
//     return;
//   }
//   // Check the existence and appropriateness of type of email input
//   if (!email || typeof email != "string") {
//     res.status(400).json({ message: "請輸入電郵地址" });
//     return;
//   }
//   if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
//     res.status(400).json({ message: "請輸入有效電郵地址" });
//     return;
//   }
//   // Check the existence and appropriateness of type of password input
//   if (!password || typeof password != "string") {
//     res.status(400).json({ message: "請輸入密碼" });
//     return;
//   }

//   // Registering as a normal user
//   if (!isVetClinicUser) {
//     // Select 4 fields of User from DB
//     let rows: Pick<User, "id" | "email" | "is_vet_clinic_user" | "is_admin">[];
//     // Obtain above selected result and catch possible DB error
//     try {
//       let result = await db.query(
//         'select id, email, is_vet_clinic_user, is_admin from "user" where username = $1',
//         [username]
//       );
//       rows = result.rows;
//     } catch (error: any) {
//       console.error("Failed to get user for registration", error);
//       res.status(500).json({ message: "Database Error" });
//       return;
//     }
//     // Obtain a user result using username
//     if (rows.length !== 0) {
//       res.status(400).json({
//         message: "此名稱已註冊，請輸入其他名稱。",
//       });
//       return;
//     }

//     // Obtain above selected result and catch possible DB error
//     try {
//       let result = await db.query(
//         'select id, username, is_vet_clinic_user, is_admin from "user" where email = $1',
//         [email]
//       );
//       rows = result.rows;
//     } catch (error: any) {
//       console.error("Failed to get user for registration", error);
//       res.status(500).json({ message: "Database Error" });
//       return;
//     }
//     // Obtain a user result using email
//     if (rows.length !== 0) {
//       res.status(400).json({
//         message: "此電郵地址已註冊，請輸入其他電郵地址。",
//       });
//       return;
//     }

//     // Check validity of password
//     if (!(password.length >= 8 && password.length <= 20)) {
//       res.status(400).json({ message: "密碼應為 8-20 個字符" });
//       return;
//     }

//     // Insert new user's information into DB
//     try {
//       const hashedPassword = await hashPassword(password);
//       await db.query(
//         `insert into "user" (username, email, hashed_password, is_vet_clinic_user, is_admin, created_at, updated_at)
//         values ($1, $2, $3, false, false, now(), now())`,
//         [username, email, hashedPassword]
//       );
//       console.log("Register successfully!");
//       // Obtain user's info in session
//       let rows: Pick<
//         User,
//         "id" | "hashed_password" | "is_vet_clinic_user" | "is_admin"
//       >[];
//       try {
//         let result = await db.query(
//           'select id, hashed_password, is_vet_clinic_user, is_admin from "user" where username = $1',
//           [username]
//         );
//         rows = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for login", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       let user = rows[0];
//       // Save user's info in session and ready to be redirected to index.html
//       req.session.user = {
//         id: user.id,
//         username: username,
//         is_vet_clinic_user: false,
//         is_admin: false,
//       };
//       req.session.save();
//       res.status(200).json({ message: "成功註冊！" });
//     } catch (error) {
//       console.error("Cannot register due to database issues");
//       res.redirect("/register.html");
//     }
//   }
//   // Registering as a vet clinic user
//   else {
//     // Check if the user selected any option except for default option
//     if (vetClinicID == -1) {
//       res.status(400).json({ message: "請選擇您的診所！" });
//       return;
//     }
//     // Check if the vet clinic is registered
//     // Vet clinic is registered
//     if (vetClinicID > 0) {
//       // Select 5 fields of User from DB
//       let rows: Pick<
//         User,
//         "id" | "email" | "is_vet_clinic_user" | "vet_clinic_id" | "is_admin"
//       >[];
//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           'select id, email, is_vet_clinic_user, is_admin from "user" where username = $1',
//           [username]
//         );
//         rows = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a user result using username
//       if (rows.length !== 0) {
//         res.status(400).json({
//           message: "此名稱已註冊，請輸入其他名稱。",
//         });
//         return;
//       }

//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           'select id, username, is_vet_clinic_user, is_admin from "user" where email = $1',
//           [email]
//         );
//         rows = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a user result using email
//       if (rows.length !== 0) {
//         res.status(400).json({
//           message: "此電郵地址已註冊，請輸入其他電郵地址。",
//         });
//         return;
//       }

//       // Check validity of password
//       if (!(password.length >= 8 && password.length <= 20)) {
//         res.status(400).json({ message: "密碼應為 8-20 個字符" });
//         return;
//       }

//       // Insert new user's information into DB
//       try {
//         const hashedPassword = await hashPassword(password);
//         await db.query(
//           `insert into "user" (username, email, hashed_password, is_vet_clinic_user, vet_clinic_id, is_admin, created_at, updated_at)
//           values ($1, $2, $3, true, $4, false, now(), now())`,
//           [username, email, hashedPassword, vetClinicID]
//         );
//         console.log("Register successfully!");
//         // Obtain user's info in session
//         let rows: Pick<
//           User,
//           "id" | "hashed_password" | "is_vet_clinic_user" | "is_admin"
//         >[];
//         try {
//           let result = await db.query(
//             'select id, hashed_password, is_vet_clinic_user, is_admin from "user" where username = $1',
//             [username]
//           );
//           rows = result.rows;
//         } catch (error: any) {
//           console.error("Failed to get user for login", error);
//           res.status(500).json({ message: "Database Error" });
//           return;
//         }
//         let user = rows[0];
//         // Save user's info in session and ready to be redirected to index.html
//         req.session.user = {
//           id: user.id,
//           username: username,
//           is_vet_clinic_user: true,
//           is_admin: false,
//         };
//         req.session.save();
//         res.status(200).json({ message: "成功註冊成為獸醫診所用戶！" });
//       } catch (error) {
//         console.error("Cannot register due to database issues (Type1)");
//         res.redirect("/register.html");
//       }
//     }
//     // Vet clinic is not registered
//     else {
//       const vetClinicName: string = req.body.vetClinicName;
//       const vetClinicAddress: string = req.body.vetClinicAddress;
//       const vetClinicDistrict: string = req.body.vetClinicDistrict;
//       const vetClinicPhone: string = req.body.vetClinicPhone;
//       const vetClinicOpeningHour: string = req.body.vetClinicOpeningHour;
//       const vetClinicClosingHour: string = req.body.vetClinicClosingHour;
//       // Select fields of Vet Clinic User from DB
//       let rowsOfVetClinic: Pick<
//         Vet_clinic,
//         | "id"
//         | "name"
//         | "address"
//         | "district"
//         | "phone"
//         | "opening_hour"
//         | "closing_hour"
//       >[];
//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           "select id, address, district, phone, opening_hour, closing_hour from vet_clinic where name = $1",
//           [vetClinicName]
//         );
//         rowsOfVetClinic = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a result using name of registered vet clinic
//       if (rowsOfVetClinic.length !== 0) {
//         res.status(400).json({
//           message:
//             "此診所名稱已註冊。請輸入其他診所名稱，或在上方選擇您的診所。",
//         });
//         return;
//       }
//       // Check the existence and appropriateness of type of username input
//       if (!vetClinicPhone || typeof vetClinicPhone != "string") {
//         res.status(400).json({ message: "請輸入電話號碼" });
//         return;
//       }
//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           "select id, name, address, district, opening_hour, closing_hour from vet_clinic where phone = $1",
//           [vetClinicPhone]
//         );
//         rowsOfVetClinic = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a result using phone of registered vet clinic
//       if (rowsOfVetClinic.length !== 0) {
//         res.status(400).json({
//           message:
//             "此電話號碼已註冊。如果您的電話號碼被佔用，請通過“聯繫我們”報告此問題，謝謝！",
//         });
//         return;
//       }

//       // Select 4 fields of User from DB
//       let rows: Pick<
//         User,
//         "id" | "email" | "is_vet_clinic_user" | "is_admin"
//       >[];
//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           'select id, email, is_vet_clinic_user, is_admin from "user" where username = $1',
//           [username]
//         );
//         rows = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a user result using username
//       if (rows.length !== 0) {
//         res.status(400).json({
//           message: "此名稱已註冊，請輸入其他名稱。",
//         });
//         return;
//       }

//       // Obtain above selected result and catch possible DB error
//       try {
//         let result = await db.query(
//           'select id, username, is_vet_clinic_user, is_admin from "user" where email = $1',
//           [email]
//         );
//         rows = result.rows;
//       } catch (error: any) {
//         console.error("Failed to get user for registration", error);
//         res.status(500).json({ message: "Database Error" });
//         return;
//       }
//       // Obtain a user result using email
//       if (rows.length !== 0) {
//         res.status(400).json({
//           message: "此電郵地址已註冊，請輸入其他電郵地址。",
//         });
//         return;
//       }

//       // Check validity of password
//       if (!(password.length >= 8 && password.length <= 20)) {
//         res.status(400).json({ message: "密碼應為 8-20 個字符" });
//         return;
//       }

//       // Insert new Vet Clinic's information into DB
//       try {
//         await db.query(
//           `insert into vet_clinic (name, address, district, phone, opening_hour, closing_hour, reservation, created_at, updated_at)
//           values ($1, $2, $3, $4, $5, $6, false, now(), now())`,
//           [
//             vetClinicName,
//             vetClinicAddress,
//             vetClinicDistrict,
//             vetClinicPhone,
//             vetClinicOpeningHour,
//             vetClinicClosingHour,
//           ]
//         );
//         console.log("Register for vet clinic successfully!");
//       } catch (error) {
//         console.error(
//           "Cannot register for vet clinic due to database issues (Type 2.1)"
//         );
//         res.redirect("/register.html");
//       }

//       // Obtain ID of newly added Vet Clinic from DB
//       try {
//         let result = await db.query(
//           `select id from vet_clinic where name = $1`,
//           [vetClinicName]
//         );
//         vetClinicID = result.rows[0]["id"];
//         // console.log(vetClinicID);
//       } catch (error) {
//         console.error(
//           "Cannot obtain ID of vet clinic user due to database issues"
//         );
//       }

//       // Insert new user's information into DB
//       try {
//         const hashedPassword = await hashPassword(password);
//         await db.query(
//           `insert into "user" (username, email, hashed_password, is_vet_clinic_user, vet_clinic_id, is_admin, created_at, updated_at)
//           values ($1, $2, $3, true, $4, false, now(), now())`,
//           [username, email, hashedPassword, vetClinicID]
//         );
//         console.log("Register for vet clinic user successfully!");
//         // Obtain user's info in session
//         let rows: Pick<
//           User,
//           "id" | "hashed_password" | "is_vet_clinic_user" | "is_admin"
//         >[];
//         try {
//           let result = await db.query(
//             'select id, hashed_password, is_vet_clinic_user, is_admin from "user" where username = $1',
//             [username]
//           );
//           rows = result.rows;
//         } catch (error: any) {
//           console.error("Failed to get user for login", error);
//           res.status(500).json({ message: "Database Error" });
//           return;
//         }
//         let user = rows[0];
//         // Save user's info in session and ready to be redirected to postdata.html
//         req.session.user = {
//           id: user.id,
//           username: username,
//           is_vet_clinic_user: true,
//           is_admin: false,
//         };
//         req.session.save();
//         res.status(200).json({
//           message: "成功註冊成為獸醫診所用戶！",
//           is_vet_clinic_user: true,
//         });
//       } catch (error) {
//         console.error(
//           "Cannot register for vet clinic user due to database issues (Type 2.2)"
//         );
//         res.redirect("/register.html");
//       }
//     }
//   }
// }

// async function login(req: Request, res: Response) {
//   const username: string = req.body.username;
//   const password: string = req.body.password;
//   // console.log({ username, password });

//   // Check the existence and appropriateness of type of username input
//   if (!username || typeof username != "string") {
//     res.status(400).json({ message: "請輸入用戶名稱" });
//     return;
//   }
//   // Check the existence and appropriateness of type of password input
//   if (!password || typeof password != "string") {
//     res.status(400).json({ message: "請輸入密碼" });
//     return;
//   }

//   // Select 4 fields of User from DB
//   let rows: Pick<
//     User,
//     "id" | "hashed_password" | "is_vet_clinic_user" | "is_admin"
//   >[];
//   // Obtain above selected result and catch possible DB error
//   try {
//     let result = await db.query(
//       'select id, hashed_password, is_vet_clinic_user, is_admin from "user" where username = $1',
//       [username]
//     );
//     rows = result.rows;
//   } catch (error: any) {
//     console.error("Failed to get user for login", error);
//     res.status(500).json({ message: "Database Error" });
//     return;
//   }

//   // Obtain no user result
//   if (rows.length == 0) {
//     res.status(400).json({
//       message: "此用戶名稱未註冊",
//     });
//     return;
//   }

//   // Failed to match the hashed password
//   let user = rows[0];
//   if (!(await checkPassword(password, user.hashed_password))) {
//     res.status(400).json({
//       message: "用戶名或密碼不正確",
//     });
//     return;
//   }

//   req.session.user = {
//     id: user.id,
//     username: username,
//     is_vet_clinic_user: user.is_vet_clinic_user,
//     is_admin: user.is_admin,
//   };
//   req.session.save();
//   res.json({ message: "登入成功！" });
// }

// async function logout(req: Request, res: Response) {
//   req.session.user = undefined;
//   req.session.save();
//   res.json({ message: "成功登出" });
// }
