import express from "express";
// import { Response, Request } from "express";
import dotenv from "dotenv";
import multer from "multer";
// import { db } from "./db";
import { loggedInOrRedirect } from "../guard";
import { medicalController } from "../server";

dotenv.config();

export const medicalRoutes = express.Router();

let fileCounter = 0;
const storage = multer.diskStorage({
  destination: "./protect/icons",
  filename: function (req, file, cb) {
    fileCounter++;
    let timestamp = Date.now();
    let ext: string = file.mimetype.split("/").pop()!;
    ext = ext.split("-").pop()!;
    ext = ext.split(";")[0];
    if (ext != "jpeg" && ext != "png" && ext != "tiff" && ext != "jpg") {
      cb(new Error("Invalid photo"), null as any);
      req.res?.status(400).end("Invalid photo, only allow jpeg format");
    } else {
      cb(null, `${file.fieldname}-${timestamp}-${fileCounter}.${ext}`);
    }
  },
});

const icon = multer({ storage: storage });

medicalRoutes.post(
  "/medical",
  loggedInOrRedirect,
  icon.single("icon"),
  medicalController.dataInsert
);

medicalRoutes.post(
  "/medical/:id",
  loggedInOrRedirect,
  icon.single("icon"),
  medicalController.dataUpdate
);

medicalRoutes.get(
  "/medical/:id",
  loggedInOrRedirect,
  medicalController.loadData
);
