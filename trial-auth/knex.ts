import env from "./env";
import Knex from "knex";

const knexConfig = require("./knexfile");

const config = Knex(knexConfig[env.NODE_ENV || "development"]);

export default config;

// export default knexConfig;

// export let knex = Knex(config);

// export const knex = Knex(knexConfig[env.NODE_ENV || "development"]);

// export const db = new Client({
//   database: process.env.DB_NAME,
//   user: process.env.DB_USERNAME,
//   password: process.env.DB_PASSWORD,
// });

// export const connectToDbPromise = db.connect((err) => {
//   if (err) {
//     console.error("database connection error", err.stack);
//   } else {
//     console.log("database connected, GOOD ahh");
//   }
// });
