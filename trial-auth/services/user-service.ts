import { Knex } from "knex";
import { User, Vet_clinic } from "../model";

export class UserService {
  constructor(private knex: Knex) {}

  getVetClinicList = async () => {
    let clinicList: Pick<Vet_clinic, "id" | "name">[];
    let result = await this.knex.raw(
      /* sql */ `select id, name from vet_clinic`
    );
    clinicList = result.rows;
    return clinicList;
  };

  checkUserByUsername = async (username: string) => {
    let userInfo: Pick<
      User,
      "id" | "email" | "is_vet_clinic_user" | "vet_clinic_id" | "is_admin"
    >[];
    let result = await this.knex.raw(
      /* sql */
      `select id, email, is_vet_clinic_user, vet_clinic_id, is_admin from "user" where username = ?`,
      [username]
    );
    userInfo = result.rows;
    return userInfo;
  };

  checkUserByEmail = async (email: string) => {
    let userInfo: Pick<
      User,
      "id" | "username" | "is_vet_clinic_user" | "vet_clinic_id" | "is_admin"
    >[];
    let result = await this.knex.raw(
      /* sql */
      `select id, username, is_vet_clinic_user, vet_clinic_id, is_admin from "user" where email = ?`,
      [email]
    );
    userInfo = result.rows;
    return userInfo;
  };

  insertNewUser = async (
    username: string,
    email: string,
    hashedPassword: string,
    vetClinicID: number | null
  ) => {
    let userId: Pick<User, "id">[];
    let result = await this.knex.raw(
      /* sql */
      `insert into "user" (username, email, hashed_password, is_vet_clinic_user, vet_clinic_id, is_admin, created_at, updated_at) 
      values (?, ?, ?, true, ?, false, now(), now())
      returning id`,
      [username, email, hashedPassword, vetClinicID]
    );
    userId = result.rows;
    return userId;
  };

  checkUserByUsernameForSessionSave = async (username: string) => {
    let userInfo: Pick<
      User,
      "id" | "hashed_password" | "is_vet_clinic_user" | "is_admin"
    >[];
    let result = await this.knex.raw(
      /* sql */
      `select id, hashed_password, is_vet_clinic_user, is_admin from "user" where username = ?`,
      [username]
    );
    userInfo = result.rows;
    return userInfo;
  };

  InsertNewUserWithVetClinic = async (row: {
    username: string;
    email: string;
    hashedPassword: string;
    vetClinicID: number;
  }) => {
    let result = await this.knex.raw(
      /* sql */
      `insert into "user" (username, email, hashed_password, is_vet_clinic_user, vet_clinic_id, is_admin, created_at, updated_at) 
      values (?, ?, ?, true, ?, false, now(), now()) 
      returning id`,
      [row.username, row.email, row.hashedPassword, row.vetClinicID]
    );
    return result.rows[0]["id"];
  };

  checkVetClinicByName = async (vetClinicName: string) => {
    let vetClinicInfo: Pick<
      Vet_clinic,
      "id" | "address" | "district" | "phone" | "opening_hour" | "closing_hour"
    >[];
    let result = await this.knex.raw(
      /* sql */
      `select id, address, district, phone, opening_hour, closing_hour from vet_clinic where name = ?`,
      [vetClinicName]
    );
    vetClinicInfo = result.rows;
    return vetClinicInfo;
  };

  checkVetClinicByPhone = async (vetClinicPhone: string) => {
    let vetClinicInfo: Pick<
      Vet_clinic,
      "id" | "name" | "address" | "district" | "opening_hour" | "closing_hour"
    >[];
    let result = await this.knex.raw(
      /* sql */
      `select id, name, address, district, opening_hour, closing_hour from vet_clinic where phone = ?`,
      [vetClinicPhone]
    );
    vetClinicInfo = result.rows;
    return vetClinicInfo;
  };

  insertNewVetClinic = async (
    vetClinicName: string,
    vetClinicAddress: string,
    vetClinicDistrict: string,
    vetClinicPhone: string,
    vetClinicOpeningHour: string,
    vetClinicClosingHour: string
  ) => {
    let result = await this.knex.raw(
      /* sql */
      `insert into vet_clinic (name, address, district, phone, opening_hour, closing_hour, reservation, created_at, updated_at)
      values (?, ?, ?, ?, ?, ?, false, now(), now())
      returning id`,
      [
        vetClinicName,
        vetClinicAddress,
        vetClinicDistrict,
        vetClinicPhone,
        vetClinicOpeningHour,
        vetClinicClosingHour,
      ]
    );
    return result.rows[0]["id"];
  };
}
