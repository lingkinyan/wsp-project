import { Knex } from "knex";

export class CommentService {
  constructor(private knex: Knex) {}

  async getComment(id: string) {
    const comment = await this.knex.raw( 
      `
        select comment_on_vet_clinic.* ,
        "user".icon,
        "user".username,
        json_agg( 
          json_build_object(
            'comment', reply_on_comment.content,
            'update',reply_on_comment.created_at, 
            'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id) 
          ) 
        ) as comment
        from comment_on_vet_clinic
        join "user"
        on comment_on_vet_clinic.user_id  = "user".id
        LEFT JOIN reply_on_comment 
        ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
        where comment_on_vet_clinic.vet_clinic_id = ?
        GROUP BY comment_on_vet_clinic.id, "user".icon, "user".username;
        `,[id]
    );
    return comment.rows;

    // const comment = await this.client.query(
    //     `
    //     select comment_on_vet_clinic.* ,
    //     "user".icon,
    //     "user".username,
    //     json_agg(
    //       json_build_object(
    //         'comment', reply_on_comment.content,
    //         'update',reply_on_comment.created_at,
    //         'username', (SELECT username FROM "user" WHERE reply_on_comment.user_id = "user".id)
    //       )
    //     ) as comment
    //     from comment_on_vet_clinic
    //     join "user"
    //     on comment_on_vet_clinic.user_id  = "user".id
    //     LEFT JOIN reply_on_comment
    //     ON comment_on_vet_clinic.id = reply_on_comment.comment_on_vet_clinic_id
    //     where comment_on_vet_clinic.vet_clinic_id = ${id}
    //     GROUP BY comment_on_vet_clinic.id, "user".icon, "user".username;
    //     `
    //     )
    //     return comment.rows
  }

  async getIntro(id:string ){

    try{
      console.log('123')
      let result = await this.knex
      .select("*")
      .from("vet_clinic")
      .where("id", id);
      return result
    }
    catch(e){
      return []
    }
  }

  async delete(id: number) {
    await this.knex("comment_on_vet_clinic").where("id", id).del();
    // await this.client.query(`delete from comment_on_vet_clinic where id=${id}`)
  }

  async put(content: string, id: number) {
    await this.knex("comment_on_vet_clinic")
      .update({ content: content })
      .where("id", id);

    // await this.client.query(`UPDATE comment_on_vet_clinic SET content = $1 where id =$2`,
    // [content,id]);
  }

  async post(
    topic: string,
    id: number,
    content: string,
    filename: string,
    user: number
  ) {
    await this.knex
      .insert([
        {
          user_id: user,
          vet_clinic_id: id,
          topic: topic,
          date: new Date(),
          content: content,
          photo: filename,
        },
      ])
      .into("comment_on_vet_clinic");

    // await this.client.query(`INSERT into comment_on_vet_clinic (user_id,vet_clinic_id,topic,date,content,photo,created_at,updated_at) values
    // ($1,$2,$3,CURRENT_DATE,$4,$5,now(),now())`,
    // [user, id, topic,content,filename])
  }

  async getReply(id: string) {
    return await this.knex.select("*").from("vet_clinic").where("id", id);
    // await this.client.query(`select * from vet_clinic where id=$1`,[id])
  }

  async postReply(
    user: number,
    comment_on_vet_clinic_id: number,
    replyContent: string
  ) {
    await this.knex
      .insert([
        {
          user_id: `${user}`,
          comment_on_vet_clinic_id: `${comment_on_vet_clinic_id}`,
          content: `${replyContent}`,
        },
      ])
      .into("reply_on_comment");

    // await this.client.query(`insert into reply_on_comment
    // (user_id,comment_on_vet_clinic_id,content,created_at,updated_at) values
    // ($1,$2,$3,now(),now())`,
    // [user,comment_on_vet_clinic_id,replyContent])
  }

  async likePost(user: number, id: number) {
    let db_likes = await this.knex
      .select("*")
      .from("likes_on_vet_clinic")
      .where("user_id", `${user}`)
      .andWhere("vet_clinic_id", `${id}`);

    // let db_likes = await this.client.query(`select * from likes_on_vet_clinic where user_id=$1 and vet_clinic_id=$2;`,
    // [user, id]);

    if (db_likes.length == 0) {
      console.log(db_likes.length);
      await this.knex
        .insert([{ user_id: `${user}`, vet_clinic_id: `${id}` }])
        .into("likes_on_vet_clinic");
    }

    // if(db_likes.rowCount == 0){
    //     await this.client.query(`INSERT into likes_on_vet_clinic (user_id,vet_clinic_id,created_at) values
    //     ($1,$2,now())`,
    //     [user,id])
    //   }
  }

  async likeGet(id: string) {
    let like = await this.knex
      .select("user_id")
      .from("likes_on_vet_clinic")
      .where("vet_clinic_id", `${id}`);
      
    return like;

    // let like = await this.client.query(`select user_id from likes_on_vet_clinic where vet_clinic_id = $1`,
    // [id])
    // return like.rows
  }
}
