import { Knex } from "knex";

export class PostDataService {
  constructor(private knex: Knex) {}

  getVetClinicID = async (user_id: number) => {
    let result = await this.knex.raw(
      /* sql */ `
              select vet_clinic_id  
              from "user"
              where id = ?
              `,
      [user_id]
    );
    return result.rows;
  };

  getVetClinicInfo = async (vetClinicID: number) => {
    let result = await this.knex.raw(
      /*sql */ `
              select * 
              from vet_clinic
              where id = ?
          `,
      [vetClinicID]
    );
    return result.rows[0];
  };

  updateVetClinicInfo = async (
    vetClinicID: number,
    name: string,
    address: string,
    district: string,
    photo: string,
    phone: string,
    openingHour: string,
    closingHour: string,
    details: string,
    reservation: boolean
  ) => {
    let result = await this.knex.raw(
      /* sql */
      `update vet_clinic
        set name = ?,
            address = ?,
            district = ?,
            photo = ?,
            phone = ?,
            opening_hour = ?,
            closing_hour = ?,
            details = ?,
            reservation = ?,
            updated_at = now()
        where id = ${vetClinicID} 
        returning id`,
      [
        name,
        address,
        district,
        photo,
        phone,
        openingHour,
        closingHour,
        details,
        reservation,
      ]
    );
    return result;
  };
}
