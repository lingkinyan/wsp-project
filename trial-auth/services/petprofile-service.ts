import { Knex } from "knex";

export class PetProfileService {
  constructor(private knex: Knex) {}

  getPetInfo = async (user_id: number) => {
    let result = await this.knex.raw(
      /* sql */ `
            select
            id
            , name
            , birthday
            , sex
            , icon
            , type
            from pet
            where user_id = (?)
            `,
      [user_id]
    );
    return result.rows;
  };
}
