import { Knex } from "knex";

export class MyAccountService {
  constructor(private knex: Knex) {}

  async get(user_id: number) {
    let result = await this.knex.raw(
      `select * from "user" where id = ${user_id}`
    );
    return result.rows;
  }

  async put(
    reviseUserName: string,
    reviseUserEmail: string,
    filename: string,
    id: number
  ) {
    await this.knex("user")
      .update({
        username: reviseUserName,
        email: reviseUserEmail,
        icon: filename,
      })
      .where("id", id);

    // await this.client.query(`UPDATE "user" SET username = $1, email = $2, icon = $3 where id = $4`,
    // [reviseUserName,reviseUserEmail,filename,id])
  }
}
