import { Knex } from "knex";

export class MedicalService {
  constructor(private knex: Knex) {}

  async dataInsert(
    user_id: number,
    name: string,
    sex: string,
    type: string,
    filename: string | null,
    breed: string,
    birthday: Date,
    weight: number,
    chipNumber: string,
    neutered: boolean,
    vacList: { name: string; date: string }[],
    medicalList: { name: string; date: string }[]
  ) {
    const result = await this.knex.raw(
      `
          insert into pet (user_id, icon, name, type, breed, sex ,birthday, neutered, weight, chip_number, created_at, updated_at) 
                    values ((?),(?),(?),(?),(?),(?), (?),(?), (?), (?), now(), now()) returning id`,
      [
        user_id,
        filename,
        name,
        type,
        breed,
        sex,
        birthday,
        neutered,
        weight,
        chipNumber,
      ]
    );

    let pet_id = result.rows[0].id;

    for (let vac of vacList) {
      await this.knex.raw(
        `insert into vaccination_record (pet_id, date, name, created_at, updated_at)
                  values ((?), (?), (?) ,now(), now())`,
        [pet_id, vac.date, vac.name]
      );
    }

    for (let history of medicalList) {
      await this.knex.raw(
        `insert into medical_history (pet_id, date, name, created_at, updated_at)
        values ((?), (?), (?), now(), now())`,
        [pet_id, history.date, history.name]
      );
    }

    return pet_id;
  }

  async dataUpdate(
    user_id: number | undefined,
    pet_id: string,
    name: string,
    sex: string,
    type: string,
    filename: string | null,
    breed: string,
    birthday: Date,
    weight: number,
    chipNumber: string,
    neutered: boolean,
    // vac: string,
    // vacdate: Date,
    // sickdate: Date,
    // history: string,
    deleteImage: string,
    vacList: { name: string; date: string }[],
    medicalList: { name: string; date: string }[]
  ) {
    let oldicon = await this.knex.raw(
      /* sql */
      `select icon from pet where id = ?`, [ pet_id ]
    );

    if(oldicon.length === 0){
      throw new Error("No target pet id.")
    }

    let oldIconResult = oldicon.rows[0].icon;

    if (!filename) {
      // 本身有icon, 唔upload
      filename = oldIconResult;
    }

    if (deleteImage === "delete") {
      filename = null;
    }

    await this.knex.raw(
      /* sql */
      `update pet
         set icon =(?),
            name = (?),
            type= (?),
            breed = (?),
            sex = (?),
            birthday =(?),
            neutered = (?),
            weight = (?), 
            chip_number = (?),
            updated_at = now()
            where id = ?`,
      [filename, name, type, breed, sex, birthday, neutered, weight, chipNumber, pet_id]
    );

    await this.knex.raw(`delete from vaccination_record where pet_id = (?)`, [
      pet_id,
    ]);

    for (let vac of vacList) {
      await this.knex.raw(
        `insert into vaccination_record (pet_id, date, name, created_at, updated_at)
                values ((?), (?), (?) ,now(), now())`,
        // [petID, vacdate, vac]
        [pet_id, vac.date, vac.name]
        // await db.query(
        //   /* sql */
        //   `update vaccination_record
        // set pet_id =$1,
        // date = $2,
        // name = $3,
        // updated_at = now()
        // where pet_id = $1`,
        //   [petID, vac.date, vac.name]
      );
    }

    await this.knex.raw(`delete from medical_history where pet_id =(?)`, [
      pet_id,
    ]);
    for (let history of medicalList) {
      await this.knex.raw(
        `insert into medical_history (pet_id, date, name, created_at, updated_at)
                values ((?), (?), (?), now(), now())`,
        [pet_id, history.date, history.name]
      );
      // /* sql */
      // `update medical_history
      // set pet_id =$1,
      // date = $2,
      // name = $3,
      // updated_at = now()
      // where pet_id = $1`,
      // [petID, history.date, history.name]
    }

    return {status: true}
  }

  async loadData(user_id: number, pet_id: string) {
    let result = await this.knex.raw(
      /* sql */ `
        select
          *
        from pet
        where pet.user_id = (?)
          and pet.id = (?)
      `,
      [user_id, pet_id]
    );

    return result.rows;
  }

  async loadMedical(pet_id: string) {
    let medicalResult = await this.knex.raw(
      /* sql */ `
            select
              pet_id
            , date
            , name
            from medical_history
            where pet_id = (?)
          `,
      [pet_id]
    );

    return medicalResult.rows;
    
  }

  async loadVac(pet_id: string) {
    let vacResult = await this.knex.raw(
      /* sql */ `
            select
              pet_id
            , date
            , name
            from vaccination_record
            where pet_id = (?)
          `,
      [pet_id]
    );

    return vacResult.rows;
      
    
  }

}
