import { Knex } from "knex";

export class ContactUsService {
  constructor(private knex: Knex) {}

  async post(name: string, email: string, content: string, phone: number) {
    await this.knex
      .insert([
        {
          name: name,
          email: email,
          content: content,
          phone: phone,
        },
      ])
      .into("contact_us_info");

    // await this.client.query(`insert into contact_us_info(name,email,content,created_at,phone) values
    // ($1,$2,$3,now(),$4)`,[name, email, content, phone])
  }
}
