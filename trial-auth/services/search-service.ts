import { Knex } from "knex";

export class SearchService {
  constructor(private knex: Knex) {}

  async getAllVetClinic() {
    let result = await this.knex.raw(
      /* sql */
      `SELECT vet_clinic.*, 
        vet_clinic_business_hour.monday_start_time AS monday_start_time,
        vet_clinic_business_hour.monday_end_time AS monday_end_time,
        vet_clinic_business_hour.monday_start_time2 AS monday_start_time2,
        vet_clinic_business_hour.monday_end_time2 AS monday_end_time2,
        vet_clinic_business_hour.tuesday_start_time AS tuesday_start_time,
        vet_clinic_business_hour.tuesday_end_time AS tuesday_end_time,
        vet_clinic_business_hour.tuesday_start_time2 AS tuesday_start_time2,
        vet_clinic_business_hour.tuesday_end_time2 AS tuesday_end_time2,
        vet_clinic_business_hour.wednesday_start_time AS wednesday_start_time,
        vet_clinic_business_hour.wednesday_end_time AS wednesday_end_time,
        vet_clinic_business_hour.wednesday_start_time2 AS wednesday_start_time2,
        vet_clinic_business_hour.wednesday_end_time2 AS wednesday_end_time2,
        vet_clinic_business_hour.thursday_start_time AS thursday_start_time,
        vet_clinic_business_hour.thursday_end_time AS thursday_end_time,
        vet_clinic_business_hour.thursday_start_time2 AS thursday_start_time2,
        vet_clinic_business_hour.thursday_end_time2 AS thursday_end_time2,
        vet_clinic_business_hour.friday_start_time AS friday_start_time,
        vet_clinic_business_hour.friday_end_time AS friday_end_time,
        vet_clinic_business_hour.friday_start_time2 AS friday_start_time2,
        vet_clinic_business_hour.friday_end_time2 AS friday_end_time2,
        vet_clinic_business_hour.saturday_start_time AS saturday_start_time,
        vet_clinic_business_hour.saturday_end_time AS saturday_end_time,
        vet_clinic_business_hour.saturday_start_time2 AS saturday_start_time2,
        vet_clinic_business_hour.saturday_end_time2 AS saturday_end_time2,
        vet_clinic_business_hour.sunday_start_time AS sunday_start_time,
        vet_clinic_business_hour.sunday_end_time AS sunday_end_time,
        vet_clinic_business_hour.sunday_start_time2 AS sunday_start_time2,
        vet_clinic_business_hour.sunday_end_time2 AS sunday_end_time2 
      FROM vet_clinic, vet_clinic_business_hour 
      WHERE vet_clinic.id = vet_clinic_business_hour.vet_clinic_id`
    );
    return result.rows;
  }
}
