import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("vet_clinic", (table) => {
    table.increments(); //id
    table.string("chinese_name");
    table.string("english_name");
    table.string("chinese_district").notNullable();
    table.string("english_district").notNullable();
    table.string("chinese_address");
    table.string("english_address");
    table.string("IP");
    table.text("phone").notNullable();
    table.string("photo");
    table.text("details");
    table.boolean("reservation");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("vet_clinic_business_hour", (table) => {
    table.increments(); //id
    table.integer("vet_clinic_id").unsigned();
    table.foreign("vet_clinic_id").references("vet_clinic.id");
    table.boolean("is_default").notNullable(); // Indicates whether the business hours are set as the default business hours (true) or not (false).
    table.string("monday_start_time");
    table.string("monday_end_time");
    table.string("monday_start_time2");
    table.string("monday_end_time2");
    table.string("tuesday_start_time");
    table.string("tuesday_end_time");
    table.string("tuesday_start_time2");
    table.string("tuesday_end_time2");
    table.string("wednesday_start_time");
    table.string("wednesday_end_time");
    table.string("wednesday_start_time2");
    table.string("wednesday_end_time2");
    table.string("thursday_start_time");
    table.string("thursday_end_time");
    table.string("thursday_start_time2");
    table.string("thursday_end_time2");
    table.string("friday_start_time");
    table.string("friday_end_time");
    table.string("friday_start_time2");
    table.string("friday_end_time2");
    table.string("saturday_start_time");
    table.string("saturday_end_time");
    table.string("saturday_start_time2");
    table.string("saturday_end_time2");
    table.string("sunday_start_time");
    table.string("sunday_end_time");
    table.string("sunday_start_time2");
    table.string("sunday_end_time2");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user", (table) => {
    table.increments(); //id
    table.string("username").notNullable();
    table.string("hashed_password");
    table.string("email").notNullable();
    table.string("icon");
    table.string("address");
    table.boolean("is_vet_clinic_user").notNullable();
    table.integer("vet_clinic_id").unsigned();
    table.foreign("vet_clinic_id").references("vet_clinic.id");
    table.boolean("is_admin").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("pet", (table) => {
    table.increments(); //id
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.string("name").notNullable();
    table.string("icon");
    table.string("type").notNullable();
    table.string("breed").notNullable();
    table.string("sex").notNullable();
    table.date("birthday");
    table.boolean("neutered");
    table.float("weight");
    table.string("chip_number");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("vet", (table) => {
    table.increments(); //id
    table.string("name").notNullable();
    table.string("photo");
    table.integer("vet_clinic_id").unsigned();
    table.foreign("vet_clinic_id").references("vet_clinic.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("medical_history", (table) => {
    table.increments(); //id
    table.integer("pet_id").unsigned();
    table.foreign("pet_id").references("pet.id");
    table.date("date").notNullable();
    table.string("name").notNullable();
    table.string("type");
    table.string("photo");
    table.text("details");
    table.integer("vet_id").unsigned;
    table.foreign("vet_id").references("vet.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("comment_on_vet_clinic", (table) => {
    table.increments(); //id
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("vet_clinic_id").unsigned();
    table.foreign("vet_clinic_id").references("vet_clinic.id");
    table.string("topic").notNullable();
    table.date("date").notNullable();
    table.string("content").notNullable();
    table.string("photo");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("vaccination_record", (table) => {
    table.increments(); //id
    table.integer("pet_id").unsigned();
    table.foreign("pet_id").references("pet.id");
    table.date("date").notNullable();
    table.string("name").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("reply_on_comment", (table) => {
    table.increments(); //id
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("comment_on_vet_clinic_id").unsigned;
    table
      .foreign("comment_on_vet_clinic_id")
      .references("comment_on_vet_clinic.id");
    table.text("content").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable("likes_on_vet_clinic", (table) => {
    table.increments(); //id
    table.integer("user_id").unsigned();
    table.foreign("user_id").references("user.id");
    table.integer("vet_clinic_id").unsigned();
    table.foreign("vet_clinic_id").references("vet_clinic.id");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("contact_us_info", (table) => {
    table.increments(); //id
    table.string("name").notNullable();
    table.string("email").notNullable();
    table.text("content").notNullable();
    table.integer("phone").notNullable();
    table.timestamps(false, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("contact_us_info");
  await knex.schema.dropTableIfExists("likes_on_vet_clinic");
  await knex.schema.dropTableIfExists("reply_on_comment");
  await knex.schema.dropTableIfExists("vaccination_record");
  await knex.schema.dropTableIfExists("comment_on_vet_clinic");
  await knex.schema.dropTableIfExists("medical_history");
  await knex.schema.dropTableIfExists("vet");
  await knex.schema.dropTableIfExists("pet");
  await knex.schema.dropTableIfExists("user");
  await knex.schema.dropTableIfExists("vet_clinic_business_hour");
  await knex.schema.dropTableIfExists("vet_clinic");
}
