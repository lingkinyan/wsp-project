import express from "express";
import { attachSession } from "./session";
import { print } from "listening-on";
import { port } from "./config";
import path from "path";
import knex from "./knex";
import http from "http";
import { Server as SocketIO } from "socket.io";
// import { connectToDbPromise } from "./db";

import { CommentService } from "./services/comment-service";
import { CommentController } from "./controllers/comment-controller";

import { UserService } from "./services/user-service";
import { UserController } from "./controllers/user-controller";

import { MedicalService } from "./services/medical-service";
import { MedicalController } from "./controllers/medical-controller";

import { SearchService } from "./services/search-service";
import { SearchController } from "./controllers/search-controller";

import { PostDataService } from "./services/postdata-service";
import { PostDataController } from "./controllers/postdata-controller";

import { MyAccountService } from "./services/myAccount-service";
import { MyAccountController } from "./controllers/myAccount-controller";

import { ContactUsService } from "./services/contactUs-service";
import { ContactUsController } from "./controllers/contactUs-controller";

import { PetProfileService } from "./services/petprofile-service";
import { PetProfileController } from "./controllers/petprofile-controller";

const app = express();

const server = new http.Server(app);
export const io = new SocketIO(server);
// Put the initialization of Services and Controller after their imports & before their corresponding routes
let userService = new UserService(knex);
export let userController = new UserController(userService);

let commentService = new CommentService(knex);
export let commentController = new CommentController(commentService);

let medicalService = new MedicalService(knex);
export let medicalController = new MedicalController(medicalService);

let searchService = new SearchService(knex)
export let searchController = new SearchController(searchService);

let postDataService = new PostDataService(knex)
export let postDataController = new PostDataController(postDataService);

let myAccountService = new MyAccountService(knex)
export let myAccountController = new MyAccountController(myAccountService);

let contactUsService = new ContactUsService(knex);
export let contactUsController = new ContactUsController(contactUsService);

let petProfileService = new PetProfileService(knex);
export let petProfileController = new PetProfileController(petProfileService);

// Import Routes after initializing their corresponding Services and Controllers
import { userRoutes } from "./routes/user-route";
import { searchRoutes } from "./routes/search-routes";
import { medicalRoutes } from "./routes/medical-route";

import { profileRoutes } from "./routes/petprofile-route";
import { postDataRoutes } from "./routes/postdata-route";
import { myAccountRoutes } from "./routes/myAccount-routes";
import { contactRoutes } from "./routes/contactUs-routes";
import { loggedIn } from "./guard";
import { commentRoutes } from "./routes/comment-routes";
import { gameRoutes } from "./routes/game-route";


app.use(express.urlencoded({ extended: false }));
app.use(express.json());

attachSession(app);
// connectToDbPromise;

app.get("/loginstatus", async (req, res) => {
  if (req.session.user) {
    res.json(req.session.user.username);
  } else {
    res.json(null);
  }
});

app.use(express.static(path.resolve("./public")));

app.use("/usericons", express.static(path.resolve("./usericons")));
app.use("/uploads", express.static(path.resolve("./uploads")));
app.use("/clinicPhoto", express.static(path.resolve("./clinicPhoto")));

app.use(userRoutes);
app.use(searchRoutes);
app.use(commentRoutes);
app.use(contactRoutes);
app.use(gameRoutes)

// with guards
app.use(profileRoutes);
app.use(medicalRoutes);
app.use(myAccountRoutes);
app.use(postDataRoutes);


app.use(loggedIn, express.static(path.resolve("./protect")));

app.use((req, res) => {
  res.status(404).sendFile(path.resolve("./public/404.html"));
});

server.listen(port, () => {
  print(port);
});
