import { Application } from "express";
import { config } from "dotenv";
import session from "express-session";
import { GrantSession } from "grant";

declare module "express-session" {
  interface SessionData {
    counter: number;
    user: {
      id: number;
      username: string;
      is_vet_clinic_user: boolean;
      is_admin: boolean;
    };
    grant?: GrantSession;
  }
}

export function attachSession(app: Application) {
  config();
  if (!process.env.SESSION_SECRET) {
    throw new Error("missing SESSION_SECRET in env");
  } 
  app.use(
    session({
      secret: process.env.SESSION_SECRET,
      resave: true,
      saveUninitialized: true,
      cookie: {
        expires: new Date(Date.now() + 360000),
        maxAge: 360000
      }
    })
  );
  console.log('session attached, NICE ahh')
}
