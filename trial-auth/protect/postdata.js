console.log("postdata.js loaded")

let updateFormVetClinic = document.querySelector("#updateFormVetClinic");

updateFormVetClinic.addEventListener("submit", async function(event) {
    event.preventDefault();

    // const formObject = {};
    let form = event.target;
    let formData = new FormData();


    formData.append('name', form.vetClinicName.value);
    formData.append('address', form.vetClinicAddress.value);
    formData.append('district', form.vetClinicDistrict.value);
    formData.append('photo', form.photo.files[0]);
    formData.append('phone', form.vetClinicPhone.value);
    formData.append('openingHour', form.vetClinicOpeningHour.value);
    formData.append('closingHour', form.vetClinicClosingHour.value);
    formData.append('details', form.vetClinicDetails.value);
    formData.append('reservation', form.vetClinicReservation.value);

    try {
        let res = await fetch('/postdata.html', {
            method: 'PUT',
            body: formData
        })
        let json = await res.json();
        if (200 <= res.status && res.status < 300) {
            loadUpdateFormStatus({ message: json.message, isSuccess: true })
            setTimeout(() => {
                window.location.href = '/search.html'
            }, 1500)
        } else {
            loadUpdateFormStatus({ message: json.message, isSuccess: false })
        }
    } catch (error) {
        loadUpdateFormStatus({ message: error.toString(), isSuccess: false })
    }
})

function loadUpdateFormStatus({ message, isSuccess }) {
    let msgBox = document.querySelector('.msg-box')
    if (isSuccess) {
        msgBox.classList.add('success')
        msgBox.classList.remove('error')
    } else {
        msgBox.classList.remove('success')
        msgBox.classList.add('error')
    }
    msgBox.textContent = message
}

async function loadVetClinicInfo () {
    try {
        let req = await fetch("/data/current/vetclinic")
        let json = await req.json();
        let currentVetClinicInfo = json.vetClinicInfo;
        updateFormVetClinic.vetClinicName.value = currentVetClinicInfo.name;
        updateFormVetClinic.vetClinicAddress.value = currentVetClinicInfo.address;
        updateFormVetClinic.vetClinicDistrict.value = currentVetClinicInfo.district;
        updateFormVetClinic.vetClinicPhone.value = currentVetClinicInfo.phone;
        updateFormVetClinic.vetClinicOpeningHour.value = currentVetClinicInfo.opening_hour;
        updateFormVetClinic.vetClinicClosingHour.value = currentVetClinicInfo.closing_hour;
        updateFormVetClinic.vetClinicDetails.value = currentVetClinicInfo.details;
        updateFormVetClinic.vetClinicReservation.value = currentVetClinicInfo.reservation;
    } catch (error) {
        res.status(500).json({ error: error.toString() })
    }
    // updateFormVetClinic.vetClinicPhoto?.filename[0] = currentVetClinicInfo.photo;
}
loadVetClinicInfo();
