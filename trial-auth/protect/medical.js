//selection of the type of animals
let pettype = document.querySelector(".pettype");
let breed = document.querySelector(".petbreed");
let vacChoice = document.querySelector(".vacChoice");
let defaultIcon = document.querySelector(".defaultIcon");
function dog() {
  breed.innerHTML = `<option value="">- 請選擇 - </option>
  <option value="poodle">貴婦犬</option>
  <option value="mongrel">唐狗</option>
  <option value="pomeranian">松鼠狗</option>
  <option value="shiba">柴犬</option>
  <option value="corgi">哥基</option>
  <option value="chihuahua">芝娃娃</option>
  <option value="schnauzer">史納莎</option>
  <option value="yorkshireTerrier">約瑟爹利</option>
  <option value="pug">八哥</option>
  <option value="shepherd">牧羊犬</option>
  <option value="other">其他</option>`;

  vacChoice.innerHTML = `<option value="">- 請選擇 - </option>
  <option value="DHPPiL">狗隻5合1疫苗</option>
  <option value="DHPPiL+">狗隻5合1疫苗加強劑</option>
  <option value="rabies">瘋狗症疫苗</option>
  <option value="cough">狗房咳疫苗</option>`;
}
function cat() {
  breed.innerHTML = `<option value="">- 請選擇 - </option>
        <option value="americanShorthair">美國短毛貓</option>
        <option value="exoticShorthair">異國短毛貓</option>
        <option value="britishShorthair">英國短毛貓</option>
        <option value="mutt">唐貓（混種貓）</option>
        <option value="ragdoll">布偶貓</option>
        <option value="persian">波斯貓</option>
        <option value="russianBlue">俄羅斯藍貓</option>
        <option value="abyssinian">阿比西尼亞貓</option>
        <option value="bengal">孟加拉豹貓</option>
        <option value="siamese">暹羅貓</option>
        <option value="burmese">緬甸貓</option>
        <option value="scottishFold">蘇格蘭折耳貓</option>
        <option value="himalayan">喜馬拉雅貓</option>
        <option value="turkeyAngora">土耳其安哥拉貓</option>
        <option value="turkishVan">土耳其梵貓</option>
        <option value="egyptianMau">埃及貓</option>
        <option value="other">其他</option>`;

  vacChoice.innerHTML = `<option value="">- 請選擇 - </option>
        <option value="FVRCP">貓隻3合1疫苗注射</option>
        <option value="FVRCP+">貓隻3合1疫苗加強劑注射</option>
        <option value="rabies">瘋狗症疫苗</option>`;
}
//review the icon
imgInp.onchange = (evt) => {
  const [file] = imgInp.files;
  if (file) {
    blah.src = URL.createObjectURL(file);
    delete_image.value = "not delete";
  }
};

//set default icon
pettype.addEventListener("change", function () {
  if (pettype.value == "dog") {
    dog();

    let iconHTTP = document.getElementById("blah").getAttribute("src");
    let text = "blob";
    if (iconHTTP.includes(text) == true) {
      return;
    } else {
      defaultIcon.setAttribute(
        "src",
        "https://i.pinimg.com/736x/d3/55/b4/d355b48c47b277a679184803e4ba1d89.jpg"
      );
    }
  }

  if (pettype.value == "cat") {
    cat();
    let iconHTTP = document.getElementById("blah").getAttribute("src");
    let text = "blob";
    if (iconHTTP.includes(text) == true) {
      return;
    } else {
      defaultIcon.setAttribute(
        "src",
        "https://static.vecteezy.com/system/resources/previews/002/787/924/original/cute-cat-paws-up-over-wall-cartoon-illustration-vector.jpg"
      );
    }
  }

  if (pettype.value == "") {
    let iconHTTP = document.getElementById("blah").getAttribute("src");
    let text = "blob";
    if (iconHTTP.includes(text) == true) {
      return;
    } else {
      defaultIcon.setAttribute(
        "src",
        "https://i.pinimg.com/originals/18/82/e0/1882e07aecdf7a3286a5013cdad5d0c0.png"
      );
    }
  }
});

// for 疫苗
let addVacBut = document.querySelector(".add-vac-but");
let vactype = document.querySelector(".vactype");
let newItemVac = document.querySelector(".newItemVac");

addVacBut.addEventListener("click", function () {
  console.log("疫苗ok");
  var node = vactype.cloneNode(true);
  document.querySelector(".newItemVac").appendChild(node);
});

// for病史
let addHistoryBut = document.querySelector(".add-history-but");
let history = document.querySelector(".historyInput");

addHistoryBut.addEventListener("click", function () {
  console.log("病史ok");
  var node = history.cloneNode(true);
  document.querySelector(".newItemHistory").appendChild(node);
});

//for save and submit the pet info
let petform = document.querySelector(".petform");
let params = new URLSearchParams(location.search);
let id = params.get("id");
petform.addEventListener("submit", async function (event) {
  event.preventDefault();
  console.log("submitted and save");
  // let vac = document.querySelector(".vacChoice")
  // let vacdate = document.querySelector(".vactype").querySelector("#date")
  // console.log(vac.value);
  // console.log(vacdate.value);
  // let history = document.querySelector("#history")
  // let historyDate = document.querySelector(".history").querySelector("#date")
  // console.log('history.value' , history.value);
  // console.log(historyDate.value);
  if (
    petname.value == "" ||
    sex.value == "" ||
    pettype.value == "" ||
    neutered.value == ""
  ) {
    Swal.fire({
      icon: "error",
      title: "Sorry!",
      text: "請重新檢查相關資料!",
    });
  } else {
    if (id == null) {
      let form = event.target;
      let formData = new FormData(form);
      // console.log({formData});
      let response = await fetch("/medical", {
        method: "POST",
        header: {
          "Content-Type": "multipart/form-data",
          Accept: "multipart/form-data",
        },
        // body: JSON.stringify(form)
        body: formData,
      });
      // let result = await response.json();
      window.location.href = "./petprofile.html";
    } else {
      let form = event.target;
      let formData = new FormData(form);
      // console.log({formData});
      let response = await fetch("/medical/" + id, {
        method: "POST",
        header: {
          "Content-Type": "multipart/form-data",
          Accept: "multipart/form-data",
        },
        // body: JSON.stringify(form)
        body: formData,
      });
      window.location.href = "./petprofile.html";
    }
  }
});

//remove the icon

let remove = document.querySelector(".remove");
let iconInput = document.querySelector(".iconInput");
let icon = document.querySelector(".icon");

imgInp.onchange = (evt) => {
  const [file] = imgInp.files;
  if (file) {
    blah.src = URL.createObjectURL(file);
  }
};

remove.addEventListener("click", function () {
  // iconInput.innerHTML = ` <input accept="image/*" type='file' id="imgInp" name='icon' id='icon' />`;
  imgInp.value = "";
  delete_image.value = "delete";

  if (pettype.value == "dog") {
    blah.src =
      "https://i.pinimg.com/736x/d3/55/b4/d355b48c47b277a679184803e4ba1d89.jpg";

    return;
  }
  if (pettype.value == "cat") {
    blah.src =
      "https://static.vecteezy.com/system/resources/previews/002/787/924/original/cute-cat-paws-up-over-wall-cartoon-illustration-vector.jpg";

    return;
  }
  if (pettype.value == "") {
    blah.src =
      "https://i.pinimg.com/originals/18/82/e0/1882e07aecdf7a3286a5013cdad5d0c0.png";
  }
});

//set the date of birthday
let calendar1 = document.querySelector(".form-control-1");
let calendar2 = document.querySelector(".form-control-2");
let calendar3 = document.querySelector(".form-control-3");

function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

calendar3.setAttribute("max", formatDate(Date()));

calendar1.setAttribute("max", formatDate(Date()));
calendar2.setAttribute("max", formatDate(Date()));

//load Data

async function loadData() {
  let params = new URLSearchParams(location.search);
  let id = params.get("id");
  let petform = document.getElementById("petform");

  if (id == null) {
    console.log("id is null");
    return;
  } else {
    let res = await fetch(`/medical/${id}`);
    let json = await res.json();
    console.log(json);

    let petDataList = document.querySelector(".petDataList");
    console.log(json);

    petform.petname.value = json.pet.name;
    petform.sex.value = json.pet.sex;
    petform.pettype.value = json.pet.type;
    petform.blah.src = `/icons/${json.pet.icon}`;
    console.log("icon:", json.pet.icon)

    if (petform.pettype.value == "dog") {
      dog();
    } else if (petform.pettype.value == "cat") {
      cat();
    }
    remove.addEventListener("click", function () {});
    if (json.pet.icon == null) {
      if (petform.pettype.value == "dog") {
        petform.blah.src =
          "https://i.pinimg.com/736x/d3/55/b4/d355b48c47b277a679184803e4ba1d89.jpg";
      }
      if (petform.pettype.value == "cat") {
        petform.blah.src =
          "https://static.vecteezy.com/system/resources/previews/002/787/924/original/cute-cat-paws-up-over-wall-cartoon-illustration-vector.jpg";
      }
    } else {
      petform.blah.src = `/icons/${json.pet.icon}`;
    }

    if (json.pet.birthday === null) {
      petform.petbday.value = "";
    } else {
      petform.petbday.value = (json.pet.birthday).slice(0,10);
    }

    petform.petbreed.value = json.pet.breed;
    petform.weight.value = json.pet.weight;
    petform.chip.value = json.pet.chip_number;
    petform.neutered.value = json.pet.neutered;

    let historylist = json.medicalList.name;
    let historyDateList = json.medicalList.date;

    function vac() {
      let vacType = document.querySelector(".newItemVac");
      let tTitle = document.querySelector(".tTitle");
      let vacChoice = document.querySelector(".vactype");
      // console.log("vaclist:", json.vacList);

      // console.log("vaclist length", json.vacList);
      // console.log("vaclist date", json.vacList[0].date.slice(0, 10));

      if (json.vacList.length == 0) {
        return;
      } else {
        for (let i = 1; i <= json.vacList.length; i++) {
          // tTitle.remove();
          vactype.remove();
          // console.log("it is the", i, "record");
          if (petform.pettype.value == "dog") {
            if (json.vacList[i - 1].name == "DHPPiL") {
              console.log("DHIPPiL");
              vacType.innerHTML +=
                /* html */
                `
                
              <tr class="vactype">
              <td>
              <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
             <option value="">- 請選擇 -</option>
             <option value="DHPPiL+">狗隻5合1疫苗加強劑</option>
             <option value="DHPPiL" SELECTED>狗隻5合1疫苗</option>
             <option value="rabies">瘋狗症疫苗</option>
            <option value="cough">狗房咳疫苗</option>
            </select>
            </td>
            <td>
            <input type="date" min="1990-01-01" max="" value = "${json.vacList[
              i - 1
            ].date.slice(
              0,
              10
            )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
            />
            </tr>
            `;
            }
            if (json.vacList[i - 1].name == "DHPPiL+") {
              console.log("DHIPPiL+");
              vacType.innerHTML +=
                /* html */
                `
              
                <tr class="vactype">
                <td>
                <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
               <option value="">- 請選擇 -</option>
               <option value="DHPPiL+" SELECTED>狗隻5合1疫苗加強劑</option>
               <option value="DHPPiL">狗隻5合1疫苗</option>
               <option value="rabies">瘋狗症疫苗</option>
              <option value="cough">狗房咳疫苗</option>
              </select>
              </td>
              <td>
              <input type="date" min="1990-01-01" max="" value = "${json.vacList[
                i - 1
              ].date.slice(
                0,
                10
              )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
              />
              </tr>
              `;
            }
            if (json.vacList[i - 1].name == "rabies") {
              console.log("rabies");
              vacType.innerHTML +=
                /* html */
                `
              <tr class="vactype">
              <td>
              <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
              <option value="">- 請選擇 -</option>
              <option value="DHPPiL" >狗隻5合1疫苗</option>
              <option value="DHPPiL+" >狗隻5合1疫苗加強劑</option>
              <option value="rabies" SELECTED>瘋狗症疫苗</option>
              <option value="cough">狗房咳疫苗</option>
              </select>
              </td>
              <td>
              <input type="date" min="1990-01-01" max="" value = "${json.vacList[
                i - 1
              ].date.slice(
                0,
                10
              )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
              />
              </tr>
             `;
            }
            if (json.vacList[i - 1].name == "cough") {
              console.log("cough");

              vacType.innerHTML +=
                /* html */
                `
              <tr class="vactype">
              <td>
              <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
              <option value="">- 請選擇 -</option>
              <option value="DHPPiL" >狗隻5合1疫苗</option>
              <option value="DHPPiL+" >狗隻5合1疫苗加強劑</option>
              <option value="rabies" >瘋狗症疫苗</option>
              <option value="cough"SELECTED>狗房咳疫苗</option>
              </select>
              </td>
              <td>
              <input type="date" min="1990-01-01" max="" value = "${json.vacList[
                i - 1
              ].date.slice(
                0,
                10
              )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
              />
              </tr>
              `;
            }
          }
          if (petform.pettype.value == "cat") {
            if (json.vacList[i - 1].name == "FVRCP") {
              console.log("FVRCP");
              vacType.innerHTML +=
                /* html */
                `
              <tr class="vactype">
              <td>
              <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
              <option value="">- 請選擇 -</option>
              <option value="FVRCP" SELECTED >貓隻3合1疫苗注射</option>
              <option value="FVRCP+">貓隻3合1疫苗加強劑注射</option>
              <option value="rabies">瘋狗症疫苗</option>
              </select>
              </td>
              <td>
              <input type="date" min="1990-01-01" max="" value = "${json.vacList[
                i - 1
              ].date.slice(
                0,
                10
              )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
              />
              </tr>
              `;
            }
            if (json.vacList[i - 1].name == "FVRCP+") {
              console.log("FVRCP+");
              vacType.innerHTML +=
                /* html */
                `
            <tr class="vactype">
            <td>
            <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
            <option value="">- 請選擇 -</option>
            <option value="FVRCP">貓隻3合1疫苗注射</option>
            <option value="FVRCP+" SELECTED>貓隻3合1疫苗加強劑注射</option>
            <option value="rabies">瘋狗症疫苗</option>
            </select>
            </td>
            <td>
            <input type="date" min="1990-01-01" max="" value = "${json.vacList[
              i - 1
            ].date.slice(
              0,
              10
            )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
            />
            </tr>
            `;
            }
            if (json.vacList[i - 1].name == "rabies") {
              console.log("rabies");
              vacType.innerHTML +=
                /* html */
                `
              <tr class="vactype">
              <td>
              <select style="height: 2.25rem; width: 16rem" name="vac" class="vacChoice" id="vacChoice">
              <option value="">- 請選擇 -</option>
              <option value="FVRCP">貓隻3合1疫苗注射</option>
              <option value="FVRCP+">貓隻3合1疫苗加強劑注射</option>
              <option value="rabies"SELECTED>瘋狗症疫苗</option>
              </select>
              </td>
              <td>
              <input type="date" min="1990-01-01" max="" value = "${json.vacList[
                i - 1
              ].date.slice(
                0,
                10
              )}" class = "form-control-2 p-1" id="date" name="vacdate" style="width: 200px"
              />
              </tr>
              `;
            }
          }
        }
      }
    }
    vac();

    let historyType = document.querySelector(".newItemHistory");
    // let historyArray = [historylist];
    // console.log(json.medicalList);
    // console.log("medical length:", json.medicalList.length);

    if (json.medicalList.length == 0) {
      return;
    } else {
      historyInput.remove();
      for (let i = 1; i < json.medicalList.length + 1; i++) {
        // console.log("this is the ", i, "record");
        let historyInput = document.querySelector(".historyInput");

        // console.log(historyType);
        // console.log("remove?");
        historyType.innerHTML += `
        <tr class="historyInput">
          <td>
            <input
              style="width: 16rem"
              type="text"
              id="history"
              name="history"
              value = "${json.medicalList[i - 1].name}"
            />
          </td>
          <td>
            <input
              type="date"
              min="1990-01-01"
              max=""
              value = "${json.medicalList[i - 1].date.slice(0, 10)}"
              class="form-control-3 p-1"
              id="date"
              name="sickdate"
              style="width: 200px"
            />
          </td>
        </tr>`;
      }
    }
  }
}

loadData();
