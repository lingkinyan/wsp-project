let petList = document.querySelector(".petList");
let petCardTemplate = document.querySelector(".petCard");

async function loadProfile() {
  let res = await fetch("/petprofile");
  let json = await res.json();
  console.log(json);
  petList.querySelectorAll(".petCard").forEach((e) => e.remove());
  for (let pet of json) {
    let petCard = petCardTemplate.cloneNode(true);

    petCard.querySelector(".petName").textContent = pet.name;
    let sex = petCard.querySelector(".sex");
    if (pet.sex == "male") {
      sex.innerHTML = `<i class="fas fa-mars"></i>`;
    } else {
      sex.innerHTML = `<i class="fas fa-venus"></i>`;
    }

    let icon = petCard.querySelector(".petIcon");
    if (pet.icon == null) {
      if (pet.type == "dog") {
        icon.innerHTML = `<img src = "https://i.pinimg.com/736x/d3/55/b4/d355b48c47b277a679184803e4ba1d89.jpg">`;
      }
      if (pet.type == "cat") {
        icon.innerHTML = `<img src = "https://static.vecteezy.com/system/resources/previews/002/787/924/original/cute-cat-paws-up-over-wall-cartoon-illustration-vector.jpg">`;
      }
    }else{
        // console.log(`<img src = "../icons/${pet.icon}">`)
        icon.innerHTML = `<img src = "/icons/${pet.icon}">`
    }

    let age;
    if (pet.birthday) {
      let year =
        (Date.now() - new Date(pet.birthday).getTime()) /
        1000 /
        60 /
        60 /
        24 /
        365.25;
      let month = (year * 12) % 12;
      year -= month / 12;
      month = Math.ceil(month);
      year = Math.round(year);
      if (year > 0) {
        age = `${year}歲${month}個月`;
      } else {
        age = `${month}個月`;
      }
      console.log(pet.birthday, age);
    }
    petCard.querySelector(".petAge").textContent = age;
    petCard.querySelector(".update-link").href = "/medical.html?id=" + pet.id;

    petList.prepend(petCard);
  }
}
loadProfile();
