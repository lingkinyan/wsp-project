console.log('clean.js')

let path = require('path')
let fs = require('fs')

let rootPath = '.'
let shouldDelete = false

for (let i = 2; i < process.argv.length; i++) {
    let arg = process.argv[i]
    if (arg == '--delete') {
        shouldDelete = true
        continue
    }
    if (arg == '--path') {
        i++
        rootPath = process.argv[i]
        continue
    }
}

console.log({
    rootPath, shouldDelete
})

let size = 0

function scanDir(dir) {
    let files = fs.readdirSync(dir)
    // console.log(files)
    files.forEach(filename => {
        let file = path.join(dir, filename)
        if (filename == 'node_modules') {
            deleteDir(file)
            return
        }
        let stat = fs.statSync(file)
        if (stat.isDirectory()) {
            scanDir(file)
        }
    })
}


function deleteDir(dir) {
    // console.log(shouldDelete ? 'Del:' : 'Found:', dir)
    if (shouldDelete) {
        console.log('Del:', dir)
    }
    let files = fs.readdirSync(dir)
    files.forEach(filename => {
        let file = path.join(dir, filename)
        let stat = fs.statSync(file)
        if (stat.isFile()) {
            size += stat.size
            if (shouldDelete) {
                fs.unlinkSync(file)
            }
            return
        }
        if (stat.isDirectory()) {
            deleteDir(file)
        }
    })
    if (shouldDelete) {
        fs.rmdirSync(dir)
    }
    process.stdout.write('\rTotal Size: ' + formatSize())
}

function formatSize() {
    if (size < 1024) {
        return size + 'B'
    }
    if (size < 1024 ** 2) {
        return (size / 1024).toFixed(1) + 'KB'
    }
    if (size < 1024 ** 3) {
        return (size / 1024 ** 2).toFixed(1) + 'MB'
    }
    return (size / 1024 ** 3).toFixed(1) + 'GB'
}

scanDir(rootPath)
process.stdout.write('\n')
