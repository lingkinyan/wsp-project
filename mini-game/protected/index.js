let countdown = document.querySelector("#countdown");
let chatroomA = document.querySelector("#simple-msg");
let joinRoom = document.querySelector("#joinRoom");
let roomChat = document.querySelector("#roomChat");
let chatSubmit = document.querySelector("#chatSubmit");
let leaveRoom = document.querySelector("#leaveRoom");
let all = document.querySelector("#all");
let groupChat = document.querySelector("#groupChat");
let chatroom = document.querySelector("#chatroom");

all.addEventListener("keydown", (event) => {
  if (event.keyCode === 13) {
    sendMessage();
    sendMessageToGroup();
    sendMessageTo();
    console.log("submit?");
  }
});

const params = new URLSearchParams(document.location.search);
const playerName = params.get("playerName");
if (!playerName) {
  // alert("你無名呀，重新登入啦！");
  fetch("/logout", { method: "POST" });
  window.location.href = "/login.html";
}
// document.querySelector("#playerName").innerText = playerName;

chatroomA.addEventListener("click", async (event) => {
  await fetch("/simplePost", {
    method: "POST",
  });
});

joinRoom.addEventListener("click", async (event) => {
  let username = playerName;
  socket.emit("join-public-room", username);
  groupChat.setAttribute("style", "display:block");
  joinRoom.setAttribute("style", "display:none");
  leaveRoom.setAttribute("style", "display:inline");
});

leaveRoom.addEventListener("click", async (event) => {
  let username = playerName;
  socket.emit("leave-room", username);
  console.log("leave room");
  groupChat.setAttribute("style", "display:none");
  joinRoom.setAttribute("style", "display:inline");
  leaveRoom.setAttribute("style", "display:none");
});

roomChat.addEventListener("click", async (event) => {
  await fetch("/to-public-group", {
    method: "POST",
  });
  // console.log("123");
});

async function postPublicMsg() {
  await fetch("/to-public-group", {
    method: "POST",
  });
}

// let toPlayer;

function sendMessage(event) {
  // event.preventDefault();
  let content = document.querySelector("#message_content").value;
  if (!`${content}` == "") {
    socket.emit("public_message", {
      content: content,
      from: playerName,
    });
  }
  // document.querySelector(
  //   "#message_display"
  // ).innerHTML += `<div>${content}</div>`;
  // document.querySelector("#message_content").value = "";
  // document.querySelector('#message_display').scrollTop = document.querySelector('#message_display').scrollHeight
}

function sendMessageTo(event) {
  // event.preventDefault();
  console.log("sender", sender);
  // console.log("sending message to :", toPlayer);
  let content = document.querySelector("#message_content1").value;
  try {
    if (!`${content}` == "") {
      if (toPlayer.id) {
        socket.emit("private_message", {
          content: content,
          to: toPlayer,
        });
      }
      document.querySelector(
        "#message_display1"
      ).innerHTML += `<div>${sender.username}:${content}</div>`;
      document.querySelector("#message_content1").value = "";
      document.querySelector("#message_display1").scrollTop =
        document.querySelector("#message_display").scrollHeight;
    }
  } catch {
    console.log("lack of toPlayer");
  }
}

function sendMessageToGroup() {
  let content = document.querySelector("#message_content2").value;
  if (!`${content}` == "") {
    socket.emit("group_message", {
      content: content,
      from: playerName,
    });
  }
}

const socket = io.connect();

socket.on("new-message", (msg) => {
  console.log(msg);
});

socket.on("connectToRoom", ({ username, id }) => {
  console.log(username);
  console.log(id);
  document.querySelector(
    "#message_display2"
  ).innerHTML += `<i><font size ="0.8">${username} has joined the room <br></font></i>`;
});

socket.on("leaveTheRoom", (username) => {
  console.log(username);
  document.querySelector(
    "#message_display2"
  ).innerHTML += `<i><font size ="0.8">${username} has left the room <br></font></i>`;
});

socket.on("new-msg-public", (data) => {
  console.log(data);
  if (!`${data.content}` == "") {
    document.querySelector(
      "#message_display"
    ).innerHTML += `<div>${data.from}: ${data.content}</div>`;
    document.querySelector("#message_display").scrollTop =
      document.querySelector("#message_display").scrollHeight;
    if (data.from == playerName) {
      document.querySelector("#message_content").value = "";
    }
  }
});

socket.on("new-msg-group", (data) => {
  console.log(data);
  if (!`${data.content}` == "") {
    document.querySelector(
      "#message_display2"
    ).innerHTML += `<div>${data.from}: ${data.content}</div>`;
    document.querySelector("#message_display2").scrollTop =
      document.querySelector("#message_display2").scrollHeight;
    if (data.from == playerName) {
      document.querySelector("#message_content2").value = "";
    }
  }
});

socket.on("new-msg-private", (data) => {
  console.log("data:", data);
  if (!`${data.content}` == "") {
    if (
      document.querySelector("#chatroom_playerName").innerHTML ===
      data.from.username
    ) {
      document.querySelector(
        "#message_display1"
      ).innerHTML += `<div>${data.from.username}: ${data.content}</div>`;
      document.querySelector("#message_display1").scrollTop =
        document.querySelector("#message_display1").scrollHeight;
    }
  }
  // socket.on('newMsgCall'),({toPlayer,sender})=>{
  //   console.log("toPlayer",toPlayer)
  document.querySelector(`#newMessageCall${data.from.id}`).innerHTML =
    "You have new message";
  // }
});

// socket.on("private_message", (data) => {
//   console.log(data);
// });

// socket.on("private_message", ({ content, from }) => {
//     if (document.querySelector('#chatroom_playerName').innerHTML === from.username) {
//       document.querySelector(
//         "#message_display"
//       ).innerHTML += `<div>${from.username}:${content}</div>`;
//       document.querySelector('#message_display').scrollTop = document.querySelector('#message_display').scrollHeight
//     }
//   });
let playerList = document.querySelector("#players");
// let scoreList = document.querySelector("#score");
let table = document.querySelector(".table");
let playerScroll = document.querySelector("#playersScroll")
// console.log('playName', playerName);
// console.log('player.username', player);
socket.on("connect", () => {
  console.log(`${playerName}socket ok左了`);
  socket.emit("client_connect", playerName);
  socket.on("allPlayerList", (roomList) => {
    showRoomList(roomList);
    socket.on("new_player", (newPlayer) => {
      console.log("have new player:", newPlayer.username);
    });
    console.log("all player list here", roomList);
  });
  socket.on("player_left", (leftPlayer) => {
    console.log("have left:", leftPlayer);
    document.querySelector(`#${leftPlayer.id}`).remove();
  });
});

function showRoomList(roomList) {
  //   console.log("roomlist should be shown");
  playerList.innerHTML = "";
  roomList.forEach((player) => {
    if ((player.username || newPlayer.username) == playerName) {
      sender = {
        id: player.id,
        username: playerName,
      };
      //   `
      //    <td>
      //    <div id = "${player.id}"> ${playerName} (me) </div>
      //   </td>
      //   <td>
      //       <div id="score">${player.score}</div>
      //   </td>

      //  `;

      playerList.innerHTML += `<div id = "${player.id}"> ${playerName} (me) </div>`;
      // scoreList.innerHTML += `${player.score}`;
      // scoreList.innerHTML = `${player.score}`;
    } else {
      //   table.innerHTML += `
      //   <td>
      //   <div id = "${player.id}"> ${player.username}<button type='button' data-username= "${player.username}"  data-player-id = "${player.id}" onclick='chat(this)' id = "chatBtn" class= 'btn'>Chat</button>
      //   <span id="newMessageCall${player.id}"></span></div>
      //  </td>
      //  <td>
      //      <div id="score">${player.score}</div>
      //  </td>

      // `;
      playerList.innerHTML += `<div id = "${player.id}"> ${player.username}<button type='button' data-username= "${player.username}"  data-player-id = "${player.id}" onclick='chat(this)' id = "chatBtn" class= 'btn'>Chat</button>
      <span id="newMessageCall${player.id}"></span></div>`;
      // scoreList.innerHTML += `${player.score}`;
      // scoreList.innerHTML = `${player.score}`;

    }
  });
}

async function chat(e) {
  toPlayer = {
    id: e.getAttribute("data-player-id"),
    username: e.getAttribute("data-username"),
  };
  document.querySelector("#chatroom_playerName").innerText =
    e.getAttribute("data-username");
  let privateMessages = await fetch("/private_message", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      toPlayer,
      sender,
    }),
  });
  privateMessages = await privateMessages.json();
  console.log("privateMessages", privateMessages);
  document.querySelector("#message_display1").innerHTML = "";
  for (let privateMessage of privateMessages) {
    console.log(privateMessage);
    document.querySelector(
      "#message_display1"
    ).innerHTML += `<div>${privateMessage.from.username}:${privateMessage.content}</div>`;
    document.querySelector("#message_display1").scrollTop =
      document.querySelector("#message_display1").scrollHeight;
  }
  // document.querySelector('#chatroom > fieldset').disabled = false
  // $("#chatroom").slideDown();
  document.querySelector(`#newMessageCall${toPlayer.id}`).innerHTML = "";
  chatroom.setAttribute("style", "display:block");
}

socket.on("photo1", (data) => {
  console.log(data);
  //loop to display five randomly chosen images at once  D
  for (let i = 0; i < 5; i++) {
    //generate a number and provide to the image to generate randomly
    //print the images generated by a random number
    document.getElementById("result").innerHTML = data.result[i];
  }
  time();
});

//mini-game
function genPhoto() {
  //declare an array to store the images
  console.log("gen photo function");
  let randomImage = new Object();
  randomImage[0] = {
    img: "https://alchetron.com/cdn/american-shorthair-f75351a7-c38b-4315-afa2-565e6715d89-resize-750.jpeg",
    answer: "american-shorthair",
  };
  randomImage[1] = {
    img: "https://alchetron.com/cdn/australian-mist-93a0ab21-043c-4ad4-8f04-bd6605614f6-resize-750.jpg",
    answer: "australian-mist",
  };
  randomImage[2] = {
    img: "https://geomedia.top/wp-content/uploads/2016/04/egeiskaya.jpg",
    answer: "aegean",
  };
  randomImage[3] = {
    img: "https://www.worldlifeexpectancy.com/images/a/d/c/b/lambkin_dwarf/lambkin_dwarf_1.jpg",
    answer: "dwarf",
  };
  randomImage[4] = {
    img: "https://www.tica.org/images/Breeds/Korat/Korat-Full-Body.jpg",
    answer: "korat",
  };
  randomImage[5] = {
    img: "https://www.pouted.com/wp-content/uploads/2015/02/The-Rare-Snowshoe-Cat-Its-Unique-Characteristics1.jpg",
    answer: "snowshoe",
  }; // loop to display five randomly chosen images at once  D
  let result = [];
  let guess = document.getElementById("guess");

  for (let i = 0; i < 6; i++) {
    //generate a number and provide to the image to generate randomly
    let number = Math.floor(Math.random() * 6);
    console.log("random number is", number);
    //print the images generated by a random number
    result.push(
      (document.getElementById("result").innerHTML =
        '<img id = "img" src="' +
        randomImage[number].img +
        '" style="width:150px" />')
    );
    if (number == 0) {
      console.log("number is 0");
      guess.innerHTML = `
        <option value = "american-shorthair" SELECTED> 美國短毛貓 </option>
        <option value = "raas"> 三色貓 </option>
        <option value = "siamese"> 暹羅貓 </option>
        <option value = "york-chocolate"> 小黑貓 </option>
        <option value = "ragdoll"> 布偶貓</option>
      `;
    }
    if (number == 1) {
      console.log("number is 1");
      guess.innerHTML = `
        <option value = "foldex"> 摺耳貓 </option>
        <option value = "dragon-li"> 狸花貓 </option>
        <option value = "australian-mist" SELECTED> 澳洲霧貓 </option>
        <option value = "egyptian-mau"> 埃及貓 </option>
        <option value = "exotic-shorthair"> 異國短毛貓</option>
      `;
    }
    if (number == 2) {
      console.log("number is 2");
      guess.innerHTML = `
        <option value = "himalayan"> 喜馬拉雅貓 </option>
        <option value = "khao-manee"> 鑽石眼貓 </option>
        <option value = "lykoi"> 狼貓 </option>
        <option value = "russian-blue"> 俄羅斯藍貓 </option>
        <option value = "aegean" SELECTED> 愛琴海貓 </option>
      `;
    }
    if (number == 3) {
      console.log("number is 3");
      guess.innerHTML = `
      <option value = "chausie"> 非洲獅子貓 </option>
      <option value = "arabian-mau"> 阿拉伯貓</option>
      <option value = "bombay"> 孟買貓</option>
      <option value = "donskoy" SELECTED> 無毛貓 </option>
      <option value = "oci"> 歐西貓 </option>
      `;
    }
    if (number == 4) {
      console.log("number is 4");
      guess.innerHTML = `
      <option value = "maine-coon"> 緬因貓 </option>
      <option value = "korat" SELECTED> 柯拉特貓</option>
      <option value = "ojos"> 麒麟貓 </option>
      <option value = "persian"> 波斯貓 </option>
      <option value = "toyger"> 虎貓 </option>
      `;
    }
    if (number == 5) {
      console.log("number is 5");
      guess.innerHTML = `
      <option value = "bicolor"> 雙色貓</option>
      <option value = "turkish-van"> 土耳其梵貓 </option>
      <option value = "kinkalow"> 矮腳貓 </option>
      <option value = "rex"> 混種短毛貓 </option>
      <option value = "snowshoe" SELECTED> 雪鞋貓</option>
      `;
    }
  }

  time();

  socket.emit("photo", {
    // randomImage : randomImage,
    result: result,
  });
}

function restart() {
  // let count = 5;
  countdown.innerHTML = "";
}

// function time(){
//     for(let i =60; i > 0 ;i--)
//     console.log(setInterval(i -= 1,1000))
// }

// function showcountdown(){
//     // let div = document.createElement('div')
//     // div.className="box"
//     // div.innerHTML = /* html */`
//     // <div>123
//     // </div>
//     // `
//     // countdown.appendChild(div)
//     countdown.innerHTML = time()
// }

function time() {
  let count = 30;
  countdown.innerHTML = "時間倒數 ： " + count;
  // let countdown = null;
  let clock = setInterval(function () {
    if (count > 0) {
      count = count - 1;
      countdown.innerHTML = "時間倒數 ： " + count;
    }
    if (count == 0) {
      countdown.innerHTML = "夠鐘";
      clearInterval(clock);
    }
  }, 1000);
}

//   });

//AI
