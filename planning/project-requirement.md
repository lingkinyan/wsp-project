## Project Requirement & Grading
# Project Requirement
## You and your team should collaborate and create interactive web applications with a frontend in JavaScript communicating to the backend in TypeScript via AJAX in RESTful APIs or socket.io with data persisted in a PostgreSQL database.

### The web application should have at least one of the following functions:

1. Online marketplace
2. Online travel package platform
3. Online O2O platform
4. Online content management system
5. Online personal management system
6. Online workflow management system
7. Online financial reporting system
8. Online gaming platform
9. Online social network platform
10. Online enterprise resource management system
11. Online SaaS (software-as-a-service) platform

### In the script, you and your team should make use of the following:

1. Callback
2. Promises
3. Async await
4. RESTful API or Socket.io
5. Express.js
6. AJAX
7. SQL
8. PostgreSQL
9. Authentication
10. new

# Project Submission and Grading
## The success of any software projects/products starts a detailed and carefully planning.

## For this project, you would need to submit the following before starting writing code for your project.

1. Wireframe of your application in images or PDF format.
2. The Entity Relation Diagrams of your tables.
3. Please submit the URL of your project Git repo with sharing with Tecky Academy Group
4. Create a board in https://project.tecky.io according to the Project Template and share it with your designated instructor. The board should be named c<cohort-number>-wsp-proj-<group-number>-<centre-location>. For example , if you are cohort 12's group 5 in Sheung Wan , then your code is c12-wsp-proj-05-sw.