## Installing Packages

### Install Jest using yarn:

```
yarn add --dev jest
```

### Using TypeScript with Jest needs some additional packages:

```
yarn add --dev typescript ts-jest @types/jest @types/node ts-node ts-node-dev
```

### Then create the config

```
yarn ts-jest config:init
```
