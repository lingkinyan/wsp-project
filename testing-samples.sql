-- insert three samples of user (5 NOT-NULL fields are inserted)
insert into "user" (username, email, hashed_password, is_admin, created_at, updated_at, is_vet_clinic_user) 
values ('testing1', 'testing1@gmail.com', 'qwertyuiop', FALSE, now(), now(), TRUE);

insert into "user" (username, email, hashed_password, is_admin, created_at, updated_at, is_vet_clinic_user) 
values ('testing2', 'testing2@gmail.com', 'asdfghjkl', FALSE, now(), now(), TRUE);

insert into "user" (username, email, hashed_password, is_admin, created_at, updated_at, is_vet_clinic_user) 
values ('testing3', 'testing3@gmail.com', 'zxcvbnm', FALSE, now(), now(), TRUE);

-- check the details in "user" table
select * from "user";

-- update the details in "user" table
update "user" set is_admin = TRUE;

-- insert two new samples of user
insert into "user" (username, email, hashed_password, is_admin, created_at, updated_at) 
values ('testing4', 'testing4@gmail.com', 'qwertyuiop', FALSE, now(), now());

insert into "user" (username, email, hashed_password, is_admin, created_at, updated_at) 
values ('testing5', 'testing5@gmail.com', 'asdfghjkl', FALSE, now(), now());

-- update again the details in "user" table
update "user" set is_admin = FALSE;

-- delete some users in "user" table with a condition
delete from "user" where id < 3;

-- remove all data in "user" table (it should be failed as there are references on "user" and references of references on "user")
truncate "user";
-- the PSQL shell should show the following ERROR: 
-- "cannot truncate a table referenced in a foreign key constraint
-- 描述:  Table "pet" references "user".
-- 提示:  Truncate table "pet" at the same time, or use TRUNCATE ... CASCADE."

-- remove all data in "user" table, along with other 4 four related tables (the id order would not be reset after inserting new data)
truncate "user", pet, comment_on_vet_clinic, comment_on_vet, medical_history;

-- delete "user" table (it should be failed as there are references on "user")
drop table "user";
-- the PSQL shell should show the following ERROR: 
-- cannot drop table "user" because other objects depend on it
-- 描述:  constraint pet_user_id_fkey on table pet depends on table "user"
-- constraint comment_on_vet_clinic_user_id_fkey on table comment_on_vet_clinic depends on table "user"
-- constraint comment_on_vet_user_id_fkey on table comment_on_vet depends on table "user"
-- 提示:  Use DROP ... CASCADE to drop the dependent objects too.

-- delete "user" table and other 4 tables
drop table "user", pet, comment_on_vet_clinic, comment_on_vet, medical_history;

-- show the list of existing tables in current database
\dt;
-- only vet and vet_clinic are shown

-- delete the whole database (go to another database before deletion)
\c postgres;
drop database pet_society;

-- You could create the database again using "db.sql"

-- Add 2 fields to "user" table
alter table "user" 
add column is_vet_clinic_user boolean not null default false,
add column vet_clinic_id INTEGER references vet_clinic(id);

-- Add 1 field to "comment_on_vet_clinic" table
alter table comment_on_vet_clinic add column reply TEXT;

-- delete 1 field to "comment_on_vet_clinic" table 
ALTER TABLE comment_on_vet_clinic DROP COLUMN reply;

-- delete "comment_on_vet" table
drop table comment_on_vet;

-- change names of 2 fields of "pet" table
alter table pet rename column type to breed;
alter table pet rename column animal to type;

-- change type of chip number in "pet" table
alter table pet alter column chip_number type VARCHAR(60);

-- Join tables of "user" and 'vet_clinic'
select "user".id as user_id, "user".username, vet_clinic.id as vet_clinic_id, vet_clinic.name as vet_clinic_name from "user" join vet_clinic on vet_clinic.id = "user".vet_clinic_id where "user".id = $1, [userID];

-- Join tables of 'vet_clinic' and 'comment_on_vet_clinic'
select * from vet_clinic right join comment_on_vet_clinic on vet_clinic.id = comment_on_vet_clinic.vet_clinic_id;

-- Insert data of table 'reply_on_comment'
insert into reply_on_comment (user_id, comment_on_vet_clinic_id, content, created_at, updated_at)
values (3, 2, 'hohoho', now(), now());

-- Insert data of table 'comment_on_vet_clinic'
insert into comment_on_vet_clinic (user_id, vet_clinic_id, topic, date, content, created_at, updated_at)
VALUES (4, 3, 'bababa', '2010-01-01', 'bulubulu', now(), now());

-- Add 1 field to "vet_clinic" table
ALTER table vet_clinic add column details TEXT;
ALTER table vet_clinic add column reservation boolean;

-- Change fields'type of "medical history" table
ALTER TABLE medical_history ALTER COLUMN type DROP NOT NULL;
ALTER TABLE medical_history ALTER COLUMN vet_id DROP NOT NULL;

-- Add 1 field to "vet_clinic" table
ALTER table vet_clinic add column likes INTEGER NOT NULL default (0);

-- Delete 1 field of "vet_clinic" table
ALTER TABLE vet_clinic DROP COLUMN likes;

-- Add 1 field in "vet_clinic" table
-- ALTER table vet_clinic ADD column likes INTEGER[] DEFAULT ARRAY[]::INTEGER[];

-- change value of 'likes' field in 'vet_clinic' table
-- UPDATE vet_clinic SET likes[1] = 3 where id = 1;
-- UPDATE vet_clinic SET likes[2:3] = '{5, 11}' where id = 1;
-- UPDATE vet_clinic SET likes[4] = 8 where id = 1;
-- UPDATE vet_clinic SET likes[2] = NULL where id = 1;
-- UPDATE vet_clinic SET likes[2] = 100 where id = 1;

-- for yanki
SET client_encoding TO 'UTF8';

-- alter table type
alter table vaccination_record alter column date type jsonb using to_jsonb(date);
alter table medical_history alter column date type jsonb using to_jsonb(date);

-- change normal user to admin account by id
UPDATE "user" SET is_admin = TRUE WHERE id = 1;
-- change normal user to admin account by username
UPDATE "user" SET is_admin = TRUE WHERE username = 'testing2';

-- Cyrus change (add 1 column phone)
alter table contact_us_info add column phone INTEGER;

歡迎親臨診所尋求寵物醫療建議。 我們有三名獸醫可以提供專業服務，包括一般醫療服務和絕育手術。