# Installation of APT (Advanced Packaging Tools)

APT manages the dependencies between different debian packages in your machine. You need to call sudo to install or remove packages from your system.

## 1. Login to the remote machine

```
ssh ubuntu@18.141.17.119
```

## [Optional checks on packages installed in my machine]

### Listing all packages installed

```
dpkg -l
```

### Search package name

```
sudo apt-cache search <package-name>
```

### Install package

```
sudo apt-get install <package-name>
```

### Remove package but keep the configuration.

```
sudo apt-get remove <package-name>
```

### Remove package and its configuration

```
sudo apt-get purge <package-name>
```

### To get the latest status of the packages.

```
sudo apt-get update
```

### To upgrade all available packages for your machine

```
sudo apt-get upgrade
```

## 2. Update all available packages for my machine

```
sudo apt-get update && sudo apt-get upgrade
```

## 3. Install NodeJS

### 3.1 Add NodeJS official repo to our list

```
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
```

### 3.2 Install nodejs

```
sudo apt-get install -y nodejs
```

## 4. Install all other necessary packages

```
sudo apt-get install build-essential nginx htop git curl postgresql postgresql-contrib ranger tree
```

## 5. Install Node packages

### 5.1 Install forever (manage our running Node)

```
sudo npm install -g forever
```

### 5.2 Install yarn

```
sudo npm install -g yarn
```
